@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">
        <div class="panel-heading text-left">
            <h2>
                <b>Permiso Renovable Para Residentes</b>
            </h2>
            <label style="font-size: 18px" class="text-pink">Paso 2 - Datos del Vehículo</label><br>
        </div>
        <form action="{{url('/Permiso/Datos_Generales')}}" id="form_finalizar" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="vehiculo" value="{{json_encode($vehiculo,true)}}">
            <input type="hidden" name="tenencias" value="{{json_encode($tenencias,true)}}">
            <input type="hidden" name="clave_vehicular" value="{{json_encode($clave_vehicular,true)}}">

            <input type="hidden" name="placa" value="{{ $placa }}">
            <input type="hidden" name="resultado" value="">

            <div class="panel-body">
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-11">
                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size: 20px"><b>MARCA:</b></label>
                                <br>
                                <label style="font-size: 20px">{{$clave_vehicular->marca}}</label>

                            </div>
                            <div class="col-md-6">
                                <label style="font-size: 20px"><b>AÑO DEL VEHÍCULO:</b></label>
                                <br>
                                <label style="font-size: 20px;color: deeppink"><b>{{$vehiculo->modelo}}</b></label>

                            </div>

                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6">
                                <label style="font-size: 20px"><b>LINEA:</b></label>
                                <br>
                                <label style="font-size: 20px">{{$clave_vehicular->linea}}</label>

                            </div>
                            <div class="col-md-6 ">
                                <label style="font-size: 20px"><b>PLACA:</b></label>
                                <br>
                                <label style="font-size: 20px;color: deeppink"><b>{{$placa}}</b></label>
                            </div>



                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6 ">
                                <label style="font-size: 20px"><b>VERSIÓN:</b></label>
                                <br>
                                <label style="font-size: 20px">{{$clave_vehicular->version}}</label>

                            </div>
                            <div class="col-md-6 ">
                                <label style="font-size: 20px"><b>TENENCIAS PAGADAS:</b></label>
                                <br>
                                <label style="font-size: 20px;color: deeppink">
                                    @if($tenencias[0]!=null)

                                        @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                                            @if(in_array($ano,$tenencias))
                                                <b>{{$ano}} </b>,
                                            @else
                                                <b style="text-decoration: line-through">{{$ano}} </b>,
                                            @endif
                                        @endfor
                                    @else
                                        @for($ano = date('Y'); $ano >= (date('Y')-5) ;$ano--  )

                                            <b style="text-decoration: line-through;">{{$ano}} </b>,

                                        @endfor
                                    @endif

                                </label>
                            </div>

                        </div>
                        <br>
                        <div class="row">


                        </div>
                        <br>
                    </div>
                </div>


            </div>
            <div class="panel-footer">
                <div class="text-center">

                    <button type="button" class="btn btn-xlg bg-teal" onclick="finalizar('REGRESAR')">
                        <i style="margin-right: 5px" class="icon  icon-undo2"></i>REGRESAR
                    </button>
                    <button type="submit" class="btn btn-xlg bg-pink" onclick="finalizar('ACEPTAR')">
                        ACEPTAR<i style="margin-left: 5px" class="icon icon-check"></i></button>
                </div>
            </div>
        </form>
    </div>

    <script type="text/javascript">
        function finalizar(resultado) {
            $('[name = resultado]').val(resultado);
            $('#form_finalizar').submit();
        }
    </script>

@endsection
