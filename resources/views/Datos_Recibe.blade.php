@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = ]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif
    <form action="{{url('/Permiso/Finalizar')}}" id="form_finalizar" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="propietario" value="{{$propietario}}">
        <input type="hidden" name="vehiculo" value="{{$vehiculo}}">
        <input type="hidden" name="tenencias" value="{{$tenencias}}">
        <input type="hidden" name="clave_vehicular" value="{{$clave_vehicular}}">
        <input type="hidden" name="placa" value="{{ $placa }}">
        <input type="hidden" name="poligono" value="{{ $poligono }}">
        <input type="hidden" name="resultado" value="">
        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b>Permiso Renovable Para Residentes</b>
                </h2>
                <label style="font-size: 18px" class="text-pink">Paso 4 - Datos de quien recibe</label><br>
            </div>
            <div class="panel-body">

                <div class="panel" style=" border: 0;box-shadow: none;">

                    <div class="panel-body">
                        <div class="text-center">
                            <label style="font-size: 20px;" class="text-pink"><b></b></label>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Primer Apellido</b></label>
                                        <input required name="primer_apellido" style="font-size: 17px" type="text"
                                               class="form-control mayusculas"
                                               autofocus>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Segundo Apellido</b></label>
                                        <input required name="segundo_apellido" style="font-size: 17px" type="text"
                                               class="form-control mayusculas">
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Nombre(s)</b></label>
                                        <input required name="nombre" style="font-size: 17px" type="text"
                                               class="form-control mayusculas">
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Parentesco</b></label>
                                        <select required style="font-size: 17px;" class="form-control" name="parentesco"
                                                id="">
                                            <option class="form-control" value=""></option>
                                            <option class="form-control" value="CONYUGE">CONYUGE</option>
                                            <option class="form-control" value="HIJO/HIJA">HIJO/HIJA</option>
                                            <option class="form-control" value="PADRE/MADRE">PADRE/MADRE</option>
                                            <option class="form-control" value="APODERADO LEGAL">APODERADO LEGAL</option>
                                            <option class="form-control" value="MISMA PERSONA">MISMA PERSONA</option>

                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-4 ">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Identificación</b></label>
                                        <select required style="font-size: 17px;" class="form-control mayusculas"
                                                name="identificacion" id="">
                                            <option class="form-control" value=""></option>
                                            <option class="form-control" value="CREDENCIAL PARA VOTAR">CREDENCIAL PARA
                                                VOTAR
                                            </option>
                                            <option class="form-control" value="CARTILLA DEL SERVICIO MILITAR NACIONAL">
                                                CARTILLA DEL SERVICIO MILITAR NACIONAL
                                            </option>
                                            <option class="form-control" value="PASAPORTE">PASAPORTE</option>
                                            <option class="form-control" value="CÉDULA PROFESIONAL">CÉDULA PROFESIONAL
                                            </option>
                                            <option class="form-control" value="LICENCIA PARA CONDUCIR">LICENCIA PARA
                                                CONDUCIR
                                            </option>
                                            <option class="form-control" value="TARJETA DE RESIDENCIA">TARJETA DE
                                                RESIDENCIA
                                            </option>
                                        </select>
                                    </div>

                                </div>


                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size: 17px;"><b>Telefono</b></label>
                                        <input name="telefono" style="font-size: 17px" type="text"
                                               class="form-control numeros">
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="text-center" style="font-size: 20px">
                                <br>
                                <label for=""><b>Comentarios</b></label>
                                <br>

                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6 col-md-offset-3">
                                    <textarea style="resize: none;height: 100px;color: black;font-size: 17px"
                                              class="form-control mayusculas" name="comentarios"
                                              placeholder="OBSERVACIONES DE REVISIÓN" maxlength="100">
                                    </textarea>
                                <span id="caracteres">100 Caracteres restantes</span>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="panel-footer">
                <div class="text-center">

                    <button type="submit" class="btn btn-xlg bg-teal" onclick="finalizar('REGRESAR')">
                        <i style="margin-right: 5px" class="icon  icon-undo2"></i>REGRESAR
                    </button>
                    <button type="submit" class="btn btn-xlg bg-pink" onclick="finalizar('FINALIZAR')">
                        FINALIZAR<i style="margin-left: 5px" class="icon icon-check"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div id="modal_zona" class="modal fade">
        <div class="modal-dialog modal-lg" style=";vertical-align: middle;margin-top: 20%;">
            <div class="modal-content">
                <div class="modal-header  text-center">
                    <h3 class="modal-title ">¿El solicitante es el beneficiario?</h3>
                </div>

                <div class="modal-body text-center">


                    <button type="button" class="btn btn-xlg bg-pink" onclick="migrar()" data-dismiss="modal">
                        SI, ES EL MISMO <i style="margin-right: 5px" class="icon icon-check2"></i>
                    </button>

                    <button type="button" class="btn btn-xlg bg-teal" data-dismiss="modal" onclick="limpiar();">
                        NO, ES OTRA PERSONA  <i style="margin-right:5px" class="icon icon-cross"></i>
                    </button>


                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {

            $('[name = comentarios]').val("");

            $('#modal_zona').modal({
                backdrop: 'static',
                keyboard: false
            });

        });

        $('[name = comentarios]').keyup(function () {
            var carac = $(this).val().length;
            var restantes = 100 - carac;
            $('#caracteres').text(restantes + " caracteres restantes.")

        });

        function migrar() {
            $('[name=parentesco]').val("MISMA PERSONA").change();
            $('[name=primer_apellido]').val("{{$primer_apellido}}");
            $('[name=segundo_apellido]').val("{{$segundo_apellido}}");
            $('[name=nombre]').val("{{$nombre}}");
            $('[name=identificacion]').focus();
        }

        function finalizar(resultado) {
            $('[name = resultado]').val(resultado);
            //$('#form_finalizar').submit();
        }
        function limpiar(){
            $('[name=parentesco]').val("").change();
            $('[name=segundo_apellido]').val("");
            $('[name=telefono]').val("");
            $('[name=identificacion]').val("").change();
            $('[name=nombre]').val("");
            $('[name=primer_apellido]').val("").focus();
        }




    </script>

@endsection
