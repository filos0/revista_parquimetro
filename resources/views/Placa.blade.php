@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = placa]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif

    <div class="panel panel-flat">
        <div class="panel-heading text-left">
            <h2>
                <b>Permiso Renovable Para Residentes</b>
            </h2>
            <label style="font-size: 18px" class="text-pink">Paso 1 - Validación de Placa</label><br>
        </div>
        <form action="{{url('/Permiso/Datos_Vehiculos')}}" id="validacion" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="panel-body">


                <div class="row ">
                    <div class="col-md-12">

                        <div class="col-md-4 col-md-offset-4 text-center">

                            <h2><b>Inserte Placa</b></h2>

                            <input type="text"
                                   style="font-size: 22px;"

                                   class="form-control text-center placa_text input-xlg"
                                   maxlength="6"
                                   name="placa"
                                   autofocus
                                   required>
                        </div>

                    </div>
                </div>
                <br>
                <br>


            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <button class="btn bg-pink btn-xlg">
                        <b>
                            BUSCAR
                            <i style="margin-left: 5px" class="icon icon-search4 "></i>
                        </b>
                    </button>
                </div>
            </div>

        </form>
    </div>

@endsection
