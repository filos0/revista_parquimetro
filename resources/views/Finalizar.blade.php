@extends('layouts.master')

@section('content')


    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            </script>
        @endforeach

    @endif

    <iframe id="printf" name="printf" src="{{url('/'.$pdf)}}" onfocus="window.close()"hidden></iframe>

    <div class="panel panel-flat">


        <div class="panel-heading text-left">
            <h2>
                <b>Permiso Renovable Para Residentes</b><br>
            </h2>
            <label class="text-pink" style="font-size: 18px">Paso 5 - Impresión</label>
        </div>
        <div class="panel-body">
            <div class="col-md-12">

                <div class="col-md-6">

                    <div class="table-responsive">
                        <table class="table table-borderless" style="width: 100%">
                            <tbody style="font-size: 20px">
                            <tr>
                                <td><b>Resultado</b></td>

                                <td class="text-teal"><b>APROBADO :)</b></td>

                            </tr>
                            <tr>
                                <td><b>Nombre</b></td>
                                <td>{{$propietario["primer_apellido"]}} {{$propietario["segundo_apellido"]}} {{$propietario["nombre"]}}</td>
                            </tr>
                            <tr>
                                <td><b>Domicilio</b></td>
                                <td>{{$domicilio}}</td>
                            </tr>
                            <tr>
                                <td><b>Fecha</b></td>
                                <td>@php echo date('d-m-Y');@endphp</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-md-5">


                    <div class="text-center" style="font-size: 22px">
                        <br>
                        <b>Imprimir Holograma</b>


                    </div>


                    <div class="text-center">
                        <br> <br>
                        <button id="btn_imprimir" onclick="imprimir_holograma()" style="font-size: 15px" type="button"
                                class="btn btn-xlg bg-teal">
                            IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                        </button>
                        <br> <br>
                        <button id="btn_salir" onclick="window.location.replace('{{url('/')}}')"
                                style="font-size: 15px" type="button"
                                class="btn btn-xlg bg-pink">
                            SALIR <i style="margin-left: 10px" class="icon icon-home"></i>
                        </button>
                    </div>


                </div>
            </div>

        </div>


    </div>

    <div id="modal_impresion" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <input type="hidden"  name="permiso_id" value="{{$permiso_id}}">

                <div class="modal-header bg-pink text-center">

                    <h2 class="modal-title">Impresión</h2>
                </div>

                <div class="modal-body">
                    <h3>¿Se imprimió correctamente?</h3>

                    <div class="row col-md-12 text-center">
                        <div class="col-md-2 col-md-offset-2">
                            <span style="font-size: 35px">SI</span>
                        </div>
                        <div class="col-md-2">
                            <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="SI">
                        </div>
                        <div class="col-md-2">
                            <span style="font-size: 35px">NO</span>
                        </div>
                        <div class="col-md-2">
                            <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="NO">
                        </div>

                    </div>
                    <br>
                    <br>

                    <div class="row col-md-12">
                        <h3>Ingresa el folio del holograma</h3>
                        <div class="col-md-8 col-md-offset-2">
                            <input type="text" style="font-size: 35px" name="folio_holograma" maxlength="5"
                                   class="form-control text-center input-xlg numeros ceros">
                        </div>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" id="btn_modal_aceptar" class="btn btn-xlg bg-pink" onclick="validar_impresion()">
                        Aceptar
                    </button>
                </div>

            </div>
        </div>
    </div>
    <script type="text/javascript">

        function disableF5(e) {
            if ((e.which || e.keyCode) === 116) e.preventDefault();
            if (e.keyCode === 82 && e.ctrlKey) e.preventDefault();
            if (e.keyCode === 37 && e.altKey) e.preventDefault();
            if (e.keyCode === 39 && e.altKey) e.preventDefault();
        };
        $(document).on("keydown", disableF5);

        $('#btn_salir').hide();

        function checar_impresion() {

            var permiso_id = $('[name = permiso_id]').val();
            $.ajax({
                url: " {{url('/API/Impresion/')}} ",
                type: "POST",
                data: {
                    "permiso_id": permiso_id,
                },success(data){
                }
            });

        }
        function imprimir_holograma() {
            checar_impresion()
            window.frames["printf"].focus();
            window.frames["printf"].print();
            $('#modal_impresion').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('[name = imp_estatus]').prop('checked', false);
            $('[name = folio_holograma]').val("").prop("disabled", true);
            $('[id= btn_modal_aceptar]').prop("disabled", true);

        }

        $('[name = imp_estatus]').on('change', function (x) {
            $('[name = folio_holograma]').val('').prop("disabled", false).focus();
            $('[id= btn_modal_aceptar]').prop("disabled", false);

        });

        function validar_impresion() {
            var impresion_estatus = $('[name = imp_estatus]:checked').val();
            var folio_holograma = $('[name = folio_holograma]').val();
            var permiso_id = $('[name = permiso_id]').val();


            if (folio_holograma == "") {
                swal({
                    title: "Debes ingresar un folio.",
                    text: "",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {
                    $('[name = folio_holograma]').focus();
                });
                return false;
            }
            else {
                $.ajax({
                    url: " {{url('/API/Validar_Folio_Holograma/')}} ",
                    type: "POST",
                    data: {
                        "impresion_estatus": impresion_estatus,
                        "folio_holograma": folio_holograma,
                        "permiso_id": permiso_id
                    },
                    success: function (data) {
                        if (data == "folio_ocupado") {
                            swal({
                                title: "El folio del holograma ya fue utilizado",
                                text: "",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#ff0005",
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                $('[name = folio_holograma]').focus();
                            });
                        }
                        else if (data == "no_hay_lote") {
                            swal({
                                title: "El folio del holograma no existe en el lote actual",
                                text: "",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#ff0005",
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                $('[name = folio_holograma]').focus();
                            });
                        }  else if (data == "incidencia") {
                            swal({
                                title: "El folio del holograma fue agregado como incidencia",
                                text: "Se imprimira de nuevo el holograma",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#ff0005",
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                imprimir_holograma();
                            });

                        }
                        else if (data == "holograma") {

                            $('#btn_imprimir').hide();
                            $('#btn_salir').show();
                            $('#modal_impresion').modal('toggle');
                            swal({
                                title: "Se término el trámite :)",
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#ff0005",
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                            });


                        }
                    }
                });
            }
        }
        function repeatString(n, string) {
            var repeat = [];
            repeat.length = n + 1;
            return repeat.join(string);
        }

        $(' .ceros').on('change', function (x) {
            var folio = $(this).val().toString();
            var tam_folio = $(this).val().length;
            var dif = 5 - tam_folio;
            var zeros = repeatString(dif, '0');
            $(this).val(zeros + folio)
            $(this).focus();
        });




    </script>





@endsection
