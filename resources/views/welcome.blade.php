@extends('layouts.master')

@section('content')

    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = ]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif


    <div class="panel">
        <div class=" text-center" style="font-size: 32px">
            <br>

            <label for=""><b>PERMISO RENOVABLE PARA RESIDENTES</b></label>
        </div>
    <!-- <div class="panel-body text-center" style="opacity:1;height: 800px;background: url('{{asset('/assets/img/discapacitado.png')}}')">-->
        <div class="panel-body">
            @if(isset($permisos))
                <br>
                <br>
                <div class="col-md-10 col-sm-offset-1">
                    @foreach($permisos->groupBy('modulo_id') as $modulo)

                        <div class="col-md-6">
                            <div id="{{$modulo->first()->modulo->id_modulo}}" style="width: 100%;height:500px;">

                            </div>
                        </div>

                    @endforeach
                </div>

            @else
                <br>
                @php
                    $today = getdate();
                    $hora=$today["hours"];
                @endphp

                @if($hora < 12)
                    <h1 class="text-pink"><b>Buenos Días</b></h1>

                @else
                    <h1 class="text-pink"><b>Buenas Tardes</b></h1>

                @endif
                <h2 style="margin-left: 50px">{{Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}} {{Auth::user()->name}}</h2>

            @endif
        </div>
    </div>
    @if(isset($permisos))
        <script type="text/javascript">
                    @foreach($permisos->groupBy('modulo_id') as $modulo)
            var myChart = echarts.init(document.getElementById('{{$modulo->first()->modulo->id_modulo}}'));

            var option = {
                title: {
                    text: 'MÓDULO {{$modulo->first()->modulo->modulo}}',
                    subtext: 'TOTAL :  {{$permisos->where('modulo_id',$modulo->first()->modulo->id_modulo)->count()}}',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient: 'vertical',
                    left: 'right',
                    data: [


                        @foreach($permisos->where('modulo_id',$modulo->first()->modulo->id_modulo)->groupBy('users_id') as $usuario)

                            '{{$usuario[0]->usuario->rfc}}',

                        @endforeach

                    ]
                },
                color:[
                    '#bf136e',
                    '#006363',
                    '#00d5ff',
                    '#ff9209'
                ],
                series: [
                    {
                        name: 'Revistas Discapacidad',
                        type: 'pie',
                        radius: '60%',
                        center: ['50%', '50%'],
                        data: [

                                @foreach($permisos->where('modulo_id',$modulo->first()->modulo->id_modulo)->groupBy('users_id') as $usuario)


                            {
                                value:{{$usuario->count()}}, name: '{{$usuario[0]->usuario->rfc}}'
                            },
                            @endforeach



                        ],
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    }
                ],
            };


            myChart.setOption(option);

            @endforeach

        </script>
    @endif
@endsection
