@extends('layouts.master')

@section('content')


    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach

    @endif
    <div class="panel panel-flat">


        <div class="panel-heading text-left">
            <h3 class=""><b>MATERIALES CARGADOS</b></h3>
        </div>
        <div class="panel-body">


            @foreach($modulos as $modulo)

                <div class="row ">
                    <div class="text-center">
                        <h3><b> MÓDULO {{$modulo->modulo}}</b></h3>
                    </div>
                </div>
                @foreach($modulo->lote_material as $lote)
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover ">
                                <thead>

                                <tr style="background-color: #E91E63;color: white">

                                    <th class="text-center">TIPO DE MATERIAL</th>
                                    <th class="text-center">FECHA ASIGANCIÓN</th>
                                    <th class="text-center">NÚMERO TOTAL</th>
                                    <th class="text-center">NÚMERO USADO</th>
                                    <th class="text-center">NÚMERO DISPONIBLE</th>
                                    <th class="text-center">INICIAL</th>
                                    <th class="text-center">FINAL</th>
                                    <th class="text-center">ESTATUS</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td class="text-center">
                                        @if($lote->cat_tipo_material_id == 1)
                                            HOLOGRAMA DE PERMISO RENOVABLE
                                        @else
                                            OTRO
                                        @endif
                                    </td>
                                    <td class="text-center"> {{$lote->fecha_asignacion }}</td>
                                    <td class="text-center"> {{$lote->total_lote }}</td>
                                    <td class="text-center"> {{$lote->usado }}</td>
                                    <td class="text-center"> {{$lote->disponible }}</td>
                                    <td class="text-center"> {{$lote->numero_rango_inicio }}</td>
                                    <td class="text-center"> {{$lote->numero_rango_fin }}</td>
                                    <td class="text-center">
                                        @if($lote->estatus == "A")
                                            <span class="label label-success">ACTIVO</span>
                                        @else
                                            <span class="label label-danger">INACTIVO</span>
                                        @endif
                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <br>
                    <br>
                @endforeach
            @endforeach


        </div>
        <br>
        <hr>
        <br>
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center">

                <label style="font-size: 18px"><b> SELECCIONA UN MÓDULO PARA CARGAR MATERIAL</b></label>

                <select name="modulo" required class="form-control input-xlg" id="modulo">
                    <option value=""></option>

                    @foreach($modulos as $modulo)
                        <option value="{{$modulo->id_modulo}}">{{$modulo->modulo}}</option>
                    @endforeach

                </select>
                <br>

            </div>

        </div>
        <div class="row">
            <div class="text-center">
                <button
                        class=" btn btn-xlg bg-teal    "
                        type="button"
                        id="agregar_btn"
                        onclick="agregarMaterial()"
                >AGREGAR MATERIAL
                </button>
            </div>
        </div>

        <br>
        <br>
        <br>
    </div>


    <div id="modal_agregarMaterial" class="modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-pink">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="row text-center">
                        <h4 class="modal-title "><b>Agregar Material</b></h4>
                    </div>
                </div>
                <form action="{{url('/Material/Agregar')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id_modulo">

                    <div class="modal-body text-center">
                        <div class="form-group">

                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <label style="font-weight: bold;font-size: 18px">Módulo</label>
                                    <input type="text" name="nombre_modulo" class="form-control text-center"
                                           readonly>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    <label style="font-weight: bold;font-size: 18px">Tipo de Material</label>
                                    <select name="tipo_material" required class="form-control">as
                                        @foreach($tipo_material as $tipo)
                                            <option class="form-control"
                                                    value="{{$tipo->id_cat_tipo_material}}">{{$tipo->tipo_material}}</option>
                                        @endforeach


                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <label style="font-weight: bold;font-size: 18px">Cantidad de Material</label>
                                    <input type="text" name="cantidad" class="form-control numeros text-center"
                                           required maxlength="5">


                                </div>

                            </div>
                            <br>
                            <div class="row">

                                <br>
                                <div class="row">

                                    <div class="col-md-4 col-md-offset-2 ">
                                        <label style="font-weight: bold;font-size: 18px">Inicial</label>
                                        <input class="form-control text-center numeros ceros" required type="text"
                                               name="rango_inicial" maxlength="5">
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-weight: bold;font-size: 18px">Final</label>
                                        <input class="form-control text-center numeros ceros" required type="text"
                                               name="rango_final" maxlength="5">
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-xlg bg-teal" data-dismiss="modal">CERRAR</button>
                        <button type="submit" class="btn btn-xlg bg-pink">APLICAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('#agregar_btn').hide();

        $('[name= modulo]').on('change', function (x) {
            $('[name=nombre_modulo]').val($("#modulo option:selected").text());
            $('[name=id_modulo]').val($("#modulo option:selected").val());
        });


        $('[name = modulo]').on('change', function () {


            let id_modulo = $('[name = modulo]').val();

            if (id_modulo !== '') {

                $('#agregar_btn').show();
            }
            else {

                $('#agregar_btn').hide();
            }


        });

        function agregarMaterial() {

            $('#modal_agregarMaterial').modal({backdrop: 'static', keyboard: false});

            $('#modal_agregarMaterial').on('shown.bs.modal', function () {


                $('[name=rango_inicial]').val('');
                $('[name=rango_final]').val('');
                $('[name=cantidad]').val('').focus();

            });
        }


        function repeatString(n, string) {
            var repeat = [];
            repeat.length = n + 1;
            return repeat.join(string);
        }

        $(' .ceros').on('change', function (x) {
            var folio = $(this).val().toString();
            var tam_folio = $(this).val().length;
            var dif = 5 - tam_folio;
            var zeros = repeatString(dif, '0');
            $(this).val(zeros + folio)
            $(this).focus();
        });

    </script>
@endsection
