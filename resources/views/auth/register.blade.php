@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$error}}",
                    text: "",
                    type: "",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach

    @endif
    <div class="panel panel-flat">


        <div class="panel-heading text-left">
            <br>
            <h3 class="panel-title text-pink"><b> AGREGAR NUEVO USUARIO</b></h3>
            <br>
        </div>
        <div class="panel-body">
            <form action="{{url('register')}}" method="POST" id="form_register">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>Módulo del Operador</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class=" icon-office"></i>
                            </button>
                        </span>
                            <select required class="form-control" name="modulo" id="" autofocus>
                                <option class="form-control" value=""></option>
                                @foreach($modulos as $modulo)
                                    <option class="form-control"
                                            value="{{$modulo->id_modulo}}"> {{$modulo->modulo}} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>
                <br>

                <div class="row">
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>Nombre del Operador</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class="icon-user"></i>
                            </button>
                        </span>
                            <input type="text" required name="nombre" maxlength="20" class="form-control mayusculas">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>Primer Apellido del Operador</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class="icon-user"></i>
                            </button>
                        </span>
                            <input type="text" required name="primer_apellido" maxlength="20"
                                   class="form-control mayusculas">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>Segundo Apellido del Operador</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class="icon-user"></i>
                            </button>
                        </span>
                            <input type="text" required name="segundo_apellido" maxlength="20"
                                   class="form-control mayusculas">
                        </div>
                    </div>
                </div>
                <br>

                <div class="row">
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>RFC del Operador</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class="icon-user"></i>
                            </button>
                        </span>
                            <input type="text" required name="rfc" class="form-control placa_text" maxlength="13">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label style="font-size: 15px" class=""><b>Rol del Usuario</b></label>
                        <div class="input-group">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-icon" type="button" disabled>
                                <i class="  icon-eye"></i>
                            </button>
                        </span>
                            <select required class="form-control" name="rol" >
                                <option class="form-control" value=""></option>
                                <option class="form-control" value="OPERADOR">OPERADOR</option>
                                <option class="form-control" value="MATERIAL">MATERIAL</option>
                                <option class="form-control" value="DIOS">DIOS</option>
                                <option class="form-control" value="JEFE">JEFE</option>
                            </select>
                        </div>
                    </div>

                </div>
                <br>

            </form>


            <div class="row">
                <div class="text-left">
                    <br>
                    <br>
                    <label style="font-size: 18px" ><code style="color: black">* La contraseña por defecto sera <b>"123456"</b> y el usuario la debera cambiar una vez que inicie sesión</code></label>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="text-center">
                <button type="button" class="btn btn-xlg bg-teal" onclick="limpiar()">Limpiar Campos</button>
                <button type="submit" form="form_register" class="btn btn-xlg bg-pink">Aceptar</button>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function limpiar() {
            $('#form_register').trigger("reset");
            $('[name = modulo]').focus();
        }


    </script>
@endsection
