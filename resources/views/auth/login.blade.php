<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EcoParq</title>

    <link href="{{ asset('/assets/remodal.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/remodal-default-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/remodal.js') }}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/media/fancybox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/components_thumbnails.js')}}"></script>
    <style type="text/css">

        html, body {
            width: 100%;
            height: 100%;
            position: fixed;
            padding: 0;
            margin: 0;
            top: 0;
            left: 0;
            background: url("{{url('/assets/img/wall2.jpg')}}") no-repeat;
            background-size: cover;
        }
    </style>
</head>
<body>
<div class="row">
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$error}}",
                    text: "",
                    type: "",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = password]').val('');
                    $('[ name = email]').val('').focus();
                });
            </script>
        @endforeach

    @endif



    <div class="col-md-3 col-md-offset-9" style="height : 100vh;background-color: white">

        <div class="panel panel-flat" style="margin-top: 40%;border: 0px ">
            <form class="form-horizontal" id="login" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="panel-heading text-center">
                    <img
                            src="{{asset('/assets/img/logo_semovi.png')}}"
                            alt="SEMOVI" width="250px">
                    <br><br>
                    <h5 style="color: deeppink"><b>PERMISO RENOVABLE PARA RESIDENTES</b></h5>
                </div>
                <div class="panel-body">

                    <div class="form-group has-feedback has-feedback-left ">
                        <input type="text" class="form-control alfanumerico" name="rfc" required
                               placeholder="RFC"
                               autofocus autocomplete="false" maxlength="13">


                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group has-feedback has-feedback-left ">
                        <input id="password" type="password" class="form-control" name="password" required
                               placeholder="Contraseña">


                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted"></i>
                        </div>
                    </div>


                    <div class="form-group">
                        <br>

                        <button type="submit" class="btn bg-pink-400 btn-block">Entrar <i
                                    class="icon-circle-right2 position-right"></i></button>
                    </div>

                </div>
            </form>

        </div>
    </div>

</div>
</body>
<script type="text/javascript">
    $('.alfanumerico').keyup(function () {

        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ@\s.,-]+/);
        $(this).val(cadena);


    });
    $(':input').focus(function () {
        $(this).css("box-shadow", "0 0 0 500px rgb(233, 30, 99,0.4) inset");
    })
        .blur(function () {
            $(this).css("box-shadow", "none");
        });
</script>
</html>

