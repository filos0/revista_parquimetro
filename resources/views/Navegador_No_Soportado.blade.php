<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EcoParq</title>
    <link rel="shortcut icon" type='image/x-icon'  href="{{asset('/assets/img/logo.ico') }}"/>

    <link href="{{asset('/assets/remodal.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/remodal-default-theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/remodal.js') }}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/media/fancybox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/components_thumbnails.js')}}"></script>

</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand" >Secretaría de Movilidad</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

</div>
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Error title -->
                <div class="text-center content-group">

                    <img width="300" height="300" src="{{url('assets/img/chrome.png')}}" alt="">
                    <br><h2 ><label for="">Navegador No Soportado</label></h2><br>
                    <h5>Esta aplicación trabaja mejor con <b>Google Chrome</b></h5>

                    <h5>Si no cuentas con el navegador lo puedes conseguir desde la <a target="_blank" href="https://www.google.com.mx/chrome/browser/desktop/">página oficial</a> o del siguiente botón  </h5><br>

                </div>
                <!-- /error title -->


                <!-- Error content -->
                <div class="row">
                    <div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">


                        <a class="btn bg-pink btn-block" href="#" target="_blank">Bajar Chrome</a>

                    </div>
                </div>
                <!-- /error wrapper -->



                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>

</body>
</html>

<script type="text/javascript">


        window.frames["printf"].focus();
        window.frames["printf"].print();
       //    setTimeout(function(){window.close();}, 1);

</script>