@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(
                    function (isConfirm) {
                        if (isConfirm) {
                            $('[name = ]').val("").focus();
                        }
                    });
            </script>
        @endforeach

    @endif


    <form action="{{url('/Permiso/Datos_Recibe')}}" id="form_finalizar" method="POST">
        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b>Permiso Renovable Para Residentes</b>
                </h2>
                <label style="font-size: 18px" class="text-pink">
                    Paso 3 - Datos Generales
                    <br>
                    Capture los <b>datos del titular</b> del permiso (no del automóvil)
                </label>
                <br>
                <label for=""></label>
            </div>
            <div class="panel-body">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="poligono" value="">
                <input type="hidden" name="resultado" value="">
                <input type="hidden" name="vehiculo" value="{{$vehiculo}}">
                <input type="hidden" name="tenencias" value="{{$tenencias}}">
                <input type="hidden" name="clave_vehicular" value="{{$clave_vehicular}}">
                <input type="hidden" name="placa" value="{{ $placa }}">
                <div class="panel" style=" border: 0;box-shadow: none;">

                    <div class="panel-body">
                        <div class="text-center">
                            <label style="font-size: 20px;" class="text-pink"><b>NOMBRE COMPLETO</b></label>
                        </div>
                        <br>


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Nombre(s)</b></label>
                                    <input name="nombre" style="font-size: 17px" type="text" autofocus
                                           class="form-control mayusculas" required>
                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Primer Apellido</b></label>
                                    <input name="primer_apellido" style="font-size: 17px" type="text"
                                           class="form-control mayusculas"
                                           required>
                                </div>

                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Segundo Apellido</b></label>
                                    <input name="segundo_apellido" style="font-size: 17px" type="text"
                                           class="form-control mayusculas" required>
                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="text-center">
                            <label style="font-size: 20px;" class="text-pink"><b>DOMICILIO</b></label>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Código Postal</b></label>
                                    <input name="codigo_postal" style="font-size: 17px" type="text" maxlength="5"
                                           class="form-control numeros" required>
                                </div>
                                <div class="text-center">
                                    <button onclick="cambiar_zona()" type="button" class="btn btn-lg bg-grey-800" id="btn_cambiar_zona">
                                        CAMBIAR ZONA <i style="margin-left: 5px"
                                                        class="glyphicon glyphicon-refresh"></i></button>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Colonia</b></label>
                                    <input name="colonia" style="font-size: 17px" type="text" class="form-control"
                                           readonly required>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Delegación</b></label>
                                    <input name="delegacion" style="font-size: 17px" type="text" class="form-control"
                                           readonly required>
                                </div>

                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Clave Zona </b></label>
                                    <input name="zona" style="font-size: 17px" type="text" class="form-control"
                                           readonly required>
                                </div>

                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Calle</b></label>
                                    <input name="calle" style="font-size: 17px" type="text" maxlength="40"
                                           class="form-control mayusculas" required>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Número Exterior</b></label>
                                    <input name="num_ext" style="font-size: 17px" type="text" maxlength="10"
                                           class="form-control mayusculas" required>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label style="font-size: 17px;"><b>Número Interior</b></label>
                                    <input name="num_int" style="font-size: 17px" type="text" maxlength="10"
                                           class="form-control mayusculas">
                                </div>

                            </div>

                        </div>

                    </div>

                </div>


            </div>
            <div class="panel-footer">
                <div class="text-center">

                    <button type="submit" class="btn btn-xlg bg-teal" onclick="finalizar('REGRESAR')">
                        <i style="margin-right: 5px" class="icon  icon-undo2"></i>REGRESAR
                    </button>
                    <button type="submit" class="btn btn-xlg bg-pink" onclick="finalizar('ACEPTAR')">ACEPTAR
                        <i style="margin-left: 5px" class="icon icon-check"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div id="modal_zona" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <br>
                    <div class="row ">
                        <div class="text-center">
                            <label style="font-size: 22px"><b>
                                    Selecciona haciendo clic en la zona que corresponda con el domicilio presentado</b></label>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">

                            <ol class="carousel-indicators" id="llenar_indicadores">

                            </ol>


                            <div class="carousel-inner" id="llenar_img">


                            </div>

                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span style="color: deeppink" class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span style="color: deeppink" class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-md-12" style="font-size: 16px">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="text-center">
                                        <label><b>CÓDIGO POSTAL</b></label><br>
                                        <label id="m_cp"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="text-center">
                                        <label><b>COLONIA</b></label><br>
                                        <label id="m_colonia"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="text-center">
                                        <label><b>DELEGACIÓN</b></label><br>
                                        <label id="m_del"></label>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                <br>


                <div class="modal-footer">
                    <button type="button" class="btn btn-xlg bg-teal" onclick="cancelar()" data-dismiss="modal">CANCELAR
                    </button>

                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        const cp = $("[name = codigo_postal]");
        const colonia = $("[name = colonia]");
        const delegacion = $("[name = delegacion]");
        const zona = $("[name = zona]");
        const btn_cambiar_zona = $('#btn_cambiar_zona');
        btn_cambiar_zona.hide();
        function limpiar_zona() {
            colonia.val("");
            delegacion.val("");
            zona.val("");
            $('[name = poligono]').val("");
        }

        function cambiar_zona() {

            const cp_bk = cp.val();
            cp.val("");
            cp.val(cp_bk).change();
            return false

        }

        function finalizar(resultado) {
            $('[name = resultado]').val(resultado);
            //$('#form_finalizar').submit();
        }

        function cancelar() {

            colonia.val('');
            delegacion.val('');
            cp.val('').focus();
        }

        $("[name = codigo_postal]").on('change', function () {
            btn_cambiar_zona.hide();

            const expreg = new RegExp('[0-9]{5}');

            if (!expreg.test(cp.val())) {

                limpiar_zona();
                swal({
                    title: "El Código Postal es Incorrecto.",
                    type: "error",
                    text: "",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    cp.val('').focus();
                });
                return false;
            }
            else {

                $.ajax({
                    url: "{{url('/API/Buscar/CP')}}",
                    type: "post",
                    data: {"cp": cp.val()},
                    success: function (poligonos) {
                        let html = "";
                        let html2 = "";
                        const base = "{{url('/poligonos')}}";

                        if (poligonos === "nada") {

                            limpiar_zona();

                            swal({
                                title: "El Código Postal NO es Valido.",
                                type: "error",
                                text: "",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                cp.val('').focus();
                            });
                            return false;
                        }

                        else {

                            $.each(poligonos, function (i, poligono) {

                                const url_img = base + '/' + poligono.clave + '.png';
                                if (i === 0) {
                                    html = html +
                                        '<div class="item active">' +
                                        '<img  style="width: 100%;cursor: pointer" src="' + url_img + '" onclick="aceptar_zona(' + poligono.id_cat_cp + ')" /> ' +
                                        '</div>';
                                    html2 = html2 +
                                        '<li data-target="#myCarousel" style=" display: inline-block;width: 20px;height: 20px;border: deeppink 2px solid" data-slide-to="' + i + '" class="active" ></li>'
                                }
                                else {
                                    html = html +
                                        '<div class="item ">' +
                                        '<img  style="width: 100%;cursor: pointer" src="' + url_img + '" onclick="aceptar_zona(' + poligono.id_cat_cp + ')" /> ' +
                                        '</div>';
                                    html2 = html2 +
                                        '<li data-target="#myCarousel" style=" display: inline-block;width: 20px;height: 20px;border: deeppink 2px solid" data-slide-to="' + i + '"  ></li>'
                                }


                            });
                            $('#llenar_indicadores').html(html2);
                            $('#llenar_img').html(html);

                            $('#modal_zona').modal({backdrop: 'static', keyboard: false});

                            $('#modal_zona').on('shown.bs.modal', function () {

                                $('#m_cp').text(poligonos[0].cp);
                                $('#m_del').text(poligonos[0].delegacion);
                                $('#m_colonia').text(poligonos[0].colonia);
                            });
                        }

                    }
                });
            }
        });


        function aceptar_zona(id_clave) {

            $.ajax({
                url: "{{url('/API/Buscar/Clave')}}",
                type: "post",
                data: {"id_clave": id_clave},
                success: function (poligono) {
                    poligono = poligono[0];

                    swal({
                        title: "¿Esta seguro que es la zona correcta?",
                        html: "" +
                        //'<img  style="width: 100%;cursor: pointer" src="' + url_img + '"/><br><br>' +
                        "<label> <b>Código Postal :</b> " + poligono.cp + "</label><br>" +
                        "<label> <b>Colonia: </b> " + poligono.colonia + "</label><br>" +
                        "<label> <b>Delegación:</b> " + poligono.delegacion + "</label><br>" +
                        "<label> <b>Zona:</b> " + poligono.clave + "</label><br>",
                        type: "warning",
                        showCancelButton: true,
                        cancelButtonText: "NO",
                        confirmButtonColor: "#dd0e00",
                        confirmButtonText: "SI",
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then(result => {

                        if (result.value) {
                            $('#modal_zona').modal('toggle');
                            colonia.val(poligono.colonia);
                            delegacion.val(poligono.delegacion);
                            zona.val(poligono.clave);
                            $('[name = poligono]').val(poligono.id_cat_cp);
                            btn_cambiar_zona.show();
                            swal({
                                title: "Zona Aceptada",
                                text: "",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#dd0e00",
                                confirmButtonText: "OK",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(result => {
                                if (result.value) {
                                    $('[name = calle]').focus();
                                }
                            });


                        } else {

                            limpiar_zona();

                            swal({
                                title: "Zona Cancelada",
                                text: "",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#dd0e00",
                                confirmButtonText: "OK",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }


                    });
                }

            });


        }


    </script>
@endsection
