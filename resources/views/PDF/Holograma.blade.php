<html>
<head>
    <style type="text/css">
        html {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', sans-serif;
            font-size: .25cm
        }

        label {
            display: inline-block;
            position: absolute;
        }

    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>
<body style="">


<img style="border: solid black 0px;position: absolute;margin-top: 11.2cm;margin-left: 4.3cm;width: 3.5cm;height: 3.2cm"
     src="{{$url_img}}" alt="">
<img style="border: solid black 0px;position: absolute;margin-top: 12cm;margin-left: 8.15cm;width: 1.9cm;height: 1.9cm"
     src="data:image/png;base64, {{ $qrcode }} " alt="">
<img style="border: solid black 0px;position: absolute;margin-top: 7.5cm;margin-left: 7.7cm;width: 2.3cm;height: 1.8cm"
     src="{{url('/assets/img/firma_completa.png')}}" alt="">


<!-------------------------------  HOLOGRAMA ------------------------------------------------->


<label style="text-align: center;margin-top: 6.4cm;margin-left: 4cm;width: 3cm;height: .6cm;background-color: white">
    <b style="font-size: .3cm">{{$placa}}</b>
</label>
<label style="text-align: center;margin-top: 6.5cm;margin-left: 7.25cm;width: .8cm;height: .6cm;background-color: white">
    <b style="font-size: .18cm;margin-top: 10px">{{$clave_poligono}}</b>
</label>
<label style="text-align: center;margin-top: 6.4cm;margin-left: 8.25cm;width: 1.8cm;height: .6cm;background-color: white">
    <b style="font-size: .3cm">{{date('d-m-Y', strtotime('+1 year'))}}</b>
</label>
<label style="text-align: center;margin-top: 10.25cm;margin-left: 6.5cm;width: 3.5cm;height: .4cm;background-color: white">
    <b style="font-size: .25cm;">{{$colonia}}</b>
</label>


<!-------------------------------  ACUSE RECIBO ------------------------------------------------->
<label style="margin-top: 5.5cm;margin-left: 16.4cm;width: 9.7cm;height: 1.6cm;background-color: white;">
    <b>NOMBRE : {{$propietario["primer_apellido"]}} {{$propietario["segundo_apellido"]}} {{$propietario["nombre"]}}
        <br>
        DOMICILIO : {{$domicilio}} </b>

</label>

<!-------------------------------  DATOS VEHICULO ------------------------------------------------->

<label style="margin-top: 7.8cm;margin-left: 16.5cm;width: 9.55cm;height: .6cm;background-color: white">
    <b  style="visibility: hidden">MARCA: </b> <b>{{$clave_vehicular["marca"]}}</b>

</label>
<label
        style="margin-top: 8.4cm;margin-left: 16.5cm;width: 9.55cm;height: .6cm;background-color: white">
    <b style="visibility: hidden">SUBMARCA: </b> <b>{{$clave_vehicular["linea"]}}</b>

</label>
<label style="margin-top: 9.1cm;margin-left: 16.5cm;width: 9.55cm;height: .6cm;background-color: white">
    <b style="visibility: hidden">MODELO: </b> <b>{{$modelo}}</b>

</label>

<label style="margin-top: 9.7cm;margin-left: 16.5cm;width: 9.55cm;height: .6cm;background-color: white">
    <b style="visibility: hidden">PLACA: </b> <b>{{$placa}}</b>

</label>

<!-------------------------------  SOLICITANTE ------------------------------------------------->
<label style="margin-top: 12.5cm;margin-left: 16.6cm;width: 9.7cm;height: .5cm;background-color: white">
    <b style="visibility: hidden">NOMBRE: </b>
    <b>{{$solicitante["primer_apellido"]}} {{$solicitante["segundo_apellido"]}} {{$solicitante["nombre"]}}</b>

</label>
<label style="margin-top: 13cm;margin-left: 16.6cm;width: 9.7cm;height: .5cm;background-color: white">
    <b style="visibility: hidden">IDENTIFICACIÓN: </b> <b>{{$solicitante["identificacion"]}}</b>

</label>
<label style="margin-top: 13.5cm;margin-left: 16.6cm;width: 9.7cm;height: .5cm;background-color: white">
    <b style="visibility: hidden">PARENTESCO: </b> <b>{{$solicitante["parentesco"]}}</b>

</label>

<label style="margin-top: 14cm;margin-left: 16.6cm;width: 4.5cm;height: .5cm;background-color: white">
    <b style="visibility: hidden">FECHA: </b> <b>{{date('d-m-Y')}}</b>

</label>
<label style="margin-top: 14cm;margin-left: 20.5cm;width: 5.2cm;height: .5cm;background-color: white">
    <b style="visibility: hidden">TELEFONO: </b> <b>{{$solicitante["telefono"]}}</b>

</label>
<label style="margin-top: 15.7cm;margin-left: 16.3cm;width: 4.7cm;height: .4cm;background-color: white">
    <b>{{substr($solicitante["comentarios"], 0 , 25)}}</b>
</label>
<label style="margin-top: 16.2cm;margin-left: 16.3cm;width: 4.7cm;height: .4cm;background-color: white">
    <b>{{substr($solicitante["comentarios"], 25 , 25)}}</b>
</label>
<label style="margin-top: 16.7cm;margin-left: 16.3cm;width: 4.7cm;height: .4cm;background-color: white">
    <b>{{substr($solicitante["comentarios"], 50 , 25)}}</b>
</label>
<label style="margin-top: 17.2cm;margin-left: 16.5cm;width: 4.7cm;height: .4cm;background-color: white">
    <b>{{substr($solicitante["comentarios"], 75 , 25)}}</b>
</label>
</body>
</html>