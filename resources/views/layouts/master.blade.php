<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>EcoParq</title>
    <link rel="shortcut icon" type='image/x-icon' href="{{asset('/assets/img/logo.ico') }}"/>


    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>


    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/echart.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>


</head>
<body>
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"></a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class=""></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li>
                <a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>

                <b><a id="hora" class="navbar-brand"></a></b>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>

                <a class="navbar-brand" style="padding: 0px;">
                    <img style="  height: 100%;padding: 5px;width: auto;"
                         src="{{asset('/assets/img/logo_semovi _2.png')}}" alt="">
                </a>

            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>
                <a class="navbar-brand" style="padding: 0px;">
                    <!--img style="  height: 100%;padding: 5px;width: auto;" src="{asset('/assets/img/cedula.jpg')}}" alt=""-->
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">

            <div class="sidebar-content">
                <div class="sidebar-user" style="cursor: pointer">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img style="width: 60px;height: 60px"
                                                                src="{{asset('/assets/img/parquimetro.png')}}"
                                                                class="img-circle  " alt=""></a>
                            <div class="media-body">
                                <div class="text-center">
                                <span class="media-heading">

                                   <b>{{Auth::user()->rfc}}</b>
                                    <br>

                                   <b>[ {{Auth::user()->rol}} ]</b>

                                </span>
                                    <div class="text-size-mini text-muted ">
                                        MÓDULO : {{Auth::user()->modulo->modulo}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar-header"></div>

                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">

                            <li>
                                <a href="{{ url('/') }}" class=" legitRipple">
                                    <i class="icon-home4"></i>
                                    <span>Inicio</span>
                                </a>
                            </li>
                            @if(Auth::user()->rol == "DIOS")

                                <li>
                                    <a href="{{url('/Permiso/Placa')}}" class="legitRipple">
                                        <i class="icon  icon-users2"></i>
                                        <span>Permiso Residentes</span>
                                    </a>
                                </li>

                                <li class="navigation-header">
                                    <span>ADMINISTRACIÓN</span>
                                    <i class="icon-menu"></i>
                                </li>

                                <li><a href="{{url('/Usuarios')}}"><i class="icon icon-users4"></i>
                                        <span>Administrar Usuarios</span></a></li>

                                <li><a href="{{url('/Material')}}"><i class=" glyphicon glyphicon-barcode"></i>
                                        <span>Administrar Material</span></a></li>

                                <li><a href="{{url('/Tramite')}}"><i class="icon icon-search4"></i>
                                        <span>Buscar Trámite</span></a></li>

                                <li class="navigation-header">
                                    <span>Reportes</span>
                                    <i class="icon-menu"></i>
                                </li>
                                <li>
                                    <a href="{{url('/Reportes/Hoy')}}"><i class="icon icon-certificate"></i>
                                        <span>Reportes de Hoy</span>
                                    </a>
                                </li>


                            @endif

                            @if(Auth::user()->rol == "OPERADOR")

                                <li>
                                    <a href="{{url('/Permiso/Placa')}}" class="legitRipple">
                                        <i class="icon  icon-users2"></i>
                                        <span>Permiso Residentes</span>
                                    </a>
                                </li>

                            @endif

                            @if(Auth::user()->rol == "MATERIAL")


                                <li class="navigation-header">
                                    <span>ADMINISTRACIÓN</span>
                                    <i class="icon-menu"></i>
                                </li>


                                <li><a href="{{url('/Material')}}"><i class=" glyphicon glyphicon-barcode"></i>
                                        <span>Administrar Material</span></a></li>



                            @endif

                            @if(Auth::user()->rol == "JEFE")


                                <li class="navigation-header">
                                    <span>ADMINISTRACIÓN</span>
                                    <i class="icon-menu"></i>
                                </li>


                                <li><a href="{{url('/Tramite')}}"><i class="icon icon-search4"></i>
                                        <span>Buscar Trámite</span></a>
                                </li>

                                <li class="navigation-header">
                                    <span>Reportes</span>
                                    <i class="icon-menu"></i>
                                </li>
                                <li>
                                    <a href="{{url('/Reportes/Hoy')}}"><i class="icon icon-certificate"></i>
                                        <span>Reportes de Hoy</span>
                                    </a>
                                </li>

                            @endif


                            <li>

                                <a href="{{ route('logout') }}" class="legitRipple"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon  icon-cross"></i>
                                    <span>Cerrar Sesión</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->
            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>

</html>
