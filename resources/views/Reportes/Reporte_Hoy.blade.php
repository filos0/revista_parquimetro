@extends('layouts.master')

@section('content')


    <div class="panel panel-flat">
        <div class="panel-heading text-center text-pink">
            <h1><b>Revistas Realizadas Hoy
                    @if($Permisos !== false)
                        [{{count($Permisos)}}]
                    @else
                        [0]
                    @endif
                </b></h1>

        </div>

        <div class="panel-body">

            <div class="row">
                <h3 style="margin-left: 50px"><b>Revistas Terminadas Correctamente</b></h3>
            </div>
            <div class="row">
                @if($Permisos !== false)
                    @foreach($Permisos as $Permiso)
                        @if($Permiso->holograma)
                            <div class="col-md-4">
                                <div class="panel bg-teal">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <label style="font-size: 20px" class=""><b>FOLIO DEL
                                                    HOLOGRAMA: {{$Permiso->holograma->folio_holograma}}</b></label>

                                        </div>
                                        <label style="font-size: 15px" class=""><b>PLACA:</b> {{$Permiso->placa}}
                                        </label>

                                        <br>
                                        <label style="font-size: 15px" class=""><b>MÓDULO
                                                :</b> {{$Permiso->modulo->modulo}}
                                        </label>
                                        <br>
                                        <label style="font-size: 15px"
                                               class=""><b>OPERADOR: </b> {{$Permiso->usuario->rfc}}
                                        </label>
                                        <br>


                                    </div>
                                    <div class="panel-footer bg-teal-800 text-right">
                                        <div class="">
                                            <b>{{$Permiso->fecha_permiso}}</b>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="row">
                <h3 style="margin-left: 50px"><b>Revistas Terminadas Sin Holograma</b></h3>
            </div>
            <div class="row">
                @if($Permisos !== false)
                    @foreach($Permisos as $Permiso)
                        @if($Permiso->holograma == null)
                            <div class="col-md-4">
                                <div class="panel bg-warning">
                                    <div class="panel-body">
                                        <div class="text-center">
                                            <label style="font-size: 20px" class=""><b> FALTA IMPRIMIR HOLOGRAMA </b>
                                            </label>

                                        </div>
                                        <label style="font-size: 15px" class=""><b>PLACA:</b> {{$Permiso->placa}}
                                        </label>

                                        <br>
                                        <label style="font-size: 15px" class=""><b>MÓDULO
                                                :</b> {{$Permiso->modulo->modulo}}
                                        </label>
                                        <br>
                                        <label style="font-size: 15px"
                                               class=""><b>OPERADOR: </b> {{$Permiso->usuario->rfc}}
                                        </label>
                                        <br>


                                    </div>
                                    <div class="panel-footer bg-warning-800 text-right">
                                        <div class="">
                                            <b>{{$Permiso->fecha_permiso}}</b>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
  
            <div class="row">
                <h3 style="margin-left: 50px"><b>Hologramas Mermados</b></h3>
            </div>
            <div class="row">
                @if($Incidencias !== false)
                    @foreach($Incidencias as $Incidencia)

                        <div class="col-md-4">
                            <div class="panel bg-danger">
                                <div class="panel-body">
                                    <div class="text-center">
                                        <label style="font-size: 20px" class=""><b>FOLIO DEL
                                                HOLOGRAMA: {{$Incidencia->folio_material}}</b> </label>

                                    </div>
                                    <br>
                                    <label style="font-size: 15px" class=""><b>OPERADOR
                                            : </b> {{$Incidencia->revista->usuario->rfc}} </label>

                                    <br>
                                    <label style="font-size: 15px" class=""><b>MÓDULO
                                            : </b>{{$Incidencia->revista->modulo->modulo}}
                                    </label>


                                </div>
                                <div class="panel-footer bg-danger-800 text-right">
                                    <div class="">
                                        <b>{{$Incidencia->fecha}}</b>
                                    </div>
                                </div>
                            </div>

                        </div>

                    @endforeach
                @endif
            </div>

        </div>
    </div>









@endsection
