@extends('layouts.master')

@section('content')


    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            </script>
        @endforeach

    @endif
    <div class="panel panel-flat">

        <div class="panel-heading text-left">

        </div>
        <div class="panel-body">
            <div class="col-md-12">
                <div class="panel ">
                    <div class="panel-heading bg-pink ">
                        <h4>AGREGAR MATERIAL</h4>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="{{url('/Material/Agregar')}}" id="form_material" method="POST">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="text-left">
                                    <br>
                                    <label style="font-size: 18px;">
                                        Para agregar material debes de llenar todos los campos del formulario
                                    </label>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Tipo De Material</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon icon-credit-card2"></i>
                                            </button>
                                        </label>
                                        <select name="tipo_material" required class="form-control">

                                            @foreach($tipo_material as $tipo)
                                                <option class="form-control"
                                                        value="{{$tipo->id_cat_tipo_material}}">{{$tipo->tipo_material}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Módulo</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon icon-home"></i>
                                            </button>
                                        </label>
                                        <select class="form-control" name="id_modulo" autofocus required>
                                            <option class="form-control" value=""></option>
                                            @foreach($modulos as $modulo)
                                                <option class="form-control"
                                                        value="{{$modulo->id_modulo}}">{{$modulo->modulo}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Cantidad de Material</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon icon-calculator3 "></i>
                                            </button>
                                        </label>
                                        <input type="text" name="cantidad" class="form-control numeros text-center"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <br>


                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Rango Inicial</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon icon-list-numbered "></i>
                                            </button>
                                        </label>
                                        <input class="form-control text-center numeros ceros" required type="text"
                                               name="rango_inicial" maxlength="6">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label style="font-size: 15px" class=""><b>Rango Final</b></label>
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <button class="btn btn-default btn-icon" type="button" disabled>
                                                <i class="icon icon-list-numbered"></i>
                                            </button>
                                        </label>
                                        <input class="form-control text-center numeros ceros" required type="text"
                                               name="rango_final" maxlength="6">
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <br>
                                <div class="text-center">
                                    <button type="button" class="btn btn-xlg bg-pink" onclick="limpiar()">LIMPIAR
                                        CAMPOS
                                    </button>
                                    <button type="submit" class="btn btn-xlg  bg-teal ">ACEPTAR</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>

                <div class="panel ">
                    <div class="panel-heading bg-pink ">
                        <h4>MATERIALES CARGADOS</h4>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>

                                <tr>

                                    <th class="text-center text-bold">MÓDULO</th>
                                    <th class="text-center text-bold">FECHA ASIGANCIÓN</th>
                                    <th class="text-center text-bold">NÚMERO TOTAL</th>
                                    <th class="text-center text-bold">NÚMERO USADO</th>
                                    <th class="text-center text-bold">NÚMERO DISPONIBLE</th>
                                    <th class="text-center text-bold">INICIAL</th>
                                    <th class="text-center text-bold">FINAL</th>
                                    <th class="text-center text-bold">ESTATUS</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($modulos as $modulo)

                                    @foreach($modulo->lote_material as $lote)
                                        <tr>
                                            <td class="text-center">{{$modulo->modulo }}</td>
                                            <td class="text-center"> {{$lote->fecha_asignacion }}</td>
                                            <td class="text-center"> {{$lote->total_lote }}</td>
                                            <td class="text-center"> {{$lote->usado }}</td>
                                            <td class="text-center"> {{$lote->disponible }}</td>
                                            <td class="text-center"> {{$lote->numero_rango_inicio }}</td>
                                            <td class="text-center"> {{$lote->numero_rango_fin }}</td>
                                            <td class="text-center">
                                                @if($lote->estatus == "A")
                                                    <span class="label label-block label-success">ACTIVO</span>
                                                @else
                                                    <span class="label label-block label-danger">INACTIVO</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                @endforeach

                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>

                <div class="panel ">
                    <div class="panel-heading bg-pink ">
                        <h4>INCIDENCIAS DE MATERIAL</h4>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="clo se"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table datatable-basic">
                                <thead>
                                <tr>
                                    <th class="text-center text-bold">FOLIO DE MATERIAL</th>
                                    <th class="text-center text-bold">MÓDULO</th>
                                    <th class="text-center text-bold">OPERADOR</th>
                                    <th class="text-center text-bold">FECHA ASIGANCIÓN</th>
                                    <th class="text-center text-bold">PLACA UTILIZADA</th>
                                    <th class="text-center text-bold">CORTESÍA UTILIZADA</th>

                                </tr>
                                </thead>
                                <tbody>


                                @foreach($incidencias as $incidencia)
                                    @if(isset($incidencia->revista))
                                        <tr>
                                            <td class="text-center">{{$incidencia->folio_material }}</td>
                                            <td class="text-center"> {{$incidencia->revista->modulo->modulo }}</td>
                                            <td class="text-center"> {{$incidencia->revista->usuario->rfc }}</td>
                                            <td class="text-center"> {{$incidencia->fecha }}</td>
                                            <td class="text-center"> {{$incidencia->revista->placa }}</td>
                                            <td class="text-center"> {{$incidencia->revista->constancia }}</td>

                                        </tr>
                                    @endif
                                @endforeach


                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>



    <script type="text/javascript">

        function limpiar() {
            $('#form_material').trigger("reset");
            $('[name = modulo]').focus();
        }

        function repeatString(n, string) {
            var repeat = [];
            repeat.length = n + 1;
            return repeat.join(string);
        }

        $(' .ceros').on('change', function (x) {
            var folio = $(this).val().toString();
            var tam_folio = $(this).val().length;
            var dif = 6 - tam_folio;
            var zeros = repeatString(dif, '0');
            $(this).val(zeros + folio)
            $(this).focus();
        });
    </script>
@endsection
