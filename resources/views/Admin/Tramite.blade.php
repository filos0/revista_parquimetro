@extends('layouts.master')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {
                    $("[name =placa ]").focus();
                });
            </script>
        @endforeach

    @endif
    @if(isset($tramite))
        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b class="text-pink">¡ TRÁMITE ENCONTRADO !</b>
                </h2>


            </div>
            <div class="panel-body">


                <div class="col-md-12">
                    <div class="col-md-4">
                        <div style="margin-top: 20%">
                            <div class="text-center">
                                <label style="font-size: 20px">
                                    <b>
                                        CLAVE POLÍGONO : {{$tramite->propietario->zona->clave}}
                                    </b>
                                </label>

                                <br>
                              <div class="col-md-8 col-md-offset-2">
                                  <img class="img-responsive"
                                       src="{{asset('/poligonos_imp/')}}/{{$tramite->propietario->zona->clave}}.png"
                                       alt="">
                              </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="text-center">
                                    <label style="font-size: 20px"><b>DATOS DE TRÁMITE </b></label>
                                </div>
                                <br>
                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td class="text-bold">FECHA DEL TRÁMITE</td>
                                        <td>{{$tramite->fecha_permiso}}</td>
                                    </tr>


                                    <tr>
                                        <td class="text-bold">PLACA</td>
                                        <td>{{$tramite->placa}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">OPERADOR</td>
                                        <td>{{$tramite->usuario->rfc}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">MÓDULO</td>
                                        <td>{{$tramite->modulo->modulo}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-bold">URL DE VALIDACIÓN</td>
                                        <td>
                                            <a target="_blank"
                                               href="http://esemovi.df.gob.mx/cvp/validar/permisoparquimetro/placa/{{$tramite->md5}}"><b>
                                                    CLICK AQUÍ</b></a>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="text-center">
                                    <label style="font-size: 20px"><b>DATOS DE PROPIETARIO </b></label>
                                </div>
                                <br>
                                <table class="table table-bordered table-hover">
                                    <tbody>

                                    <tr>
                                        <td class="text-bold">NOMBRE</td>
                                        <td colspan="3"> {{($tramite->propietario->nombre)?? "-"}} {{($tramite->propietario->primer_apellido)??"-"}} {{($tramite->propietario->segundo_apellido)??"-"}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">DELEGACIÓN</td>
                                        <td> {{$tramite->propietario->zona->delegacion}}</td>
                                        <td class="text-bold">CÓDIGO POSTAL</td>
                                        <td>  {{$tramite->propietario->zona->cp}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-bold">COLONIA</td>
                                        <td colspan="3"> {{$tramite->propietario->zona->colonia}}</td>

                                    </tr>
                                    <tr>
                                        <td class="text-bold">CALLE</td>
                                        <td colspan="3"> {{$tramite->propietario->calle}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">NÚMERO EXTERIOR</td>
                                        <td> {{$tramite->propietario->num_ext}}</td>
                                        <td class="text-bold">NÚMERO INTERIOR</td>
                                        <td> {{$tramite->propietario->num_int}}</td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="text-center">
                                    <label style="font-size: 20px">
                                        <b>
                                            {{($tramite->holograma)? "HOLOGRAMA IMPRESO": "NO SE IMPRIMIO HOLOGRAMA"}}
                                        </b>
                                    </label>
                                </div>
                                <br>
                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <td class="text-bold">FOLIO HOLOGRAMA</td>
                                        <td>{{($tramite->holograma->folio_holograma)?? "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">FECHA EXPEDIDO</td>
                                        <td>{{($tramite->holograma->fecha_holograma)?? "-"}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-bold">FECHA VIGENCIA</td>
                                        <td>{{($tramite->holograma->vigencia_documento)?? "-"}}</td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>


                </div>



            </div>
            <div class="panel-footer">
                <div class="text-center">
                    <a class="btn btn-xlg bg-teal-700" href="{{url("/Tramite")}}">
                        <i class="icon icon-arrow-left12"></i> REGRESAR
                    </a>
                    <button type="button" class="btn btn-xlg bg-danger-700" onclick="cancelar()">
                        <i class="icon icon-cross3"></i> CANCELAR TRÁMITE
                    </button>

                </div>
            </div>
        </div>

        <div id="modal_confirmar" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form id="modal_form" action="{{url("/Tramite/Cancelar")}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id_revista" value="{{$tramite->id_permiso_propietario}}">

                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group">
                                    <div class="text-center">
                                        <span style="font-size: 25px" class="text-bold text-danger-800"> ¿Estas seguro de cancelar el trámite?</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div style="font-size: 15px">
                                    <div class="form-group">
                                        <div class="col md-12">

                                            <div class="text-left">
                                                <label style="font-size: 25px">Inserta el folio del oficio</label>
                                            </div>
                                            <br>
                                            <div class="text-center ">
                                                <input name="folio_oficio " type="text" id="folio_oficio"
                                                       class="form-control mayusculas_todo input-xlg"
                                                       placeholder="DCVLP-999-2018">
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-xlg bg-danger-800" data-dismiss="modal">CANCELAR
                                </button>
                                <button type="submit" class="btn btn-xlg bg-success-800" id="btn_confirmar">CONFIRMAR
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $("#btn_confirmar").attr("disabled", true);

            function cancelar() {
                $('#modal_confirmar').modal({backdrop: 'static', keyboard: false});
                $('#modal_confirmar').on('shown.bs.modal', function () {
                    $("#folio_oficio").val("").focus();
                });
            }

            $("#folio_oficio").on('keyup', function (x) {

                if ($(this).val().length < 5)
                    $("#btn_confirmar").attr("disabled", true);
                else
                    $("#btn_confirmar").attr("disabled", false);
            })
        </script>

    @else

        <div class="panel panel-flat">
            <div class="panel-heading text-left">
                <h2>
                    <b>Información de un Trámite </b>
                </h2>
                <label style="font-size: 20px" class="text-pink"><b>Ingresa la fecha y la placa ocupada en el
                        trámite</b></label>
                <br>
                <label style="font-size: 18px"></label>

            </div>
            <div class="panel-body">
                <form action="{{url('/Tramite')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row ">
                        <div class="col-md-12">

                            <div class="col-md-4 col-md-offset-2 text-center">

                                <h2>Fecha de Alta<br>
                                    <small></small>
                                </h2>
                                <input type="date" class="form-control text-center input-xlg"
                                       min="2018-01-01" max="{{date('Y')}}-12-31"
                                       value="{{date('Y-m-d')}}"
                                       name="fecha_alta" autofocus required>

                            </div>
                            <div class="col-md-4 text-center">

                                <h2>Inserte placa <br>
                                    <small></small>
                                </h2>
                                <input type="text" class="form-control text-center placa_text input-xlg"
                                       maxlength="6"
                                       name="placa" required>

                            </div>

                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="row">
                        <div class="text-center">
                            <button class="btn bg-pink btn-xlg">
                                BUSCAR <i style="margin-left: 5px" class="icon icon-search4 "></i>
                            </button>
                        </div>
                    </div>


                </form>

            </div>


        </div>
    @endif

@endsection
