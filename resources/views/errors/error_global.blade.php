<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Revista Discapacitados</title>


    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/datatables_responsive.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('/assets/js/plugins/tables/datatables/extensions/responsive.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/media/fancybox.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/components_thumbnails.js')}}"></script>
    <style type="text/css">

        html, body {
            width: 100%;
            height: 100%;
            background-color: white;
            position:fixed;
            padding:0;
            margin:0;
            top:0;
            left:0;
        }


    </style>
</head>
<body>

<div class="row">
    <div class="col-md-8 " style="height : 100vh;">
        <div style="margin-left: 100px;margin-top: 200px">
            <label style="font-size: 50px" class="text-bold text-pink">
                OPS! <br> OCURRIO UN ERROR.
            </label>
            <br>
            <label style="font-size: 20px" class="text-bold">
                <code style="color: black;">
                    {{$msg_1}}
                </code>

            </label>
            <br>
            <label style="font-size: 20px" class="text-bold">
                <code style="color: black;">
                    {{$msg_2}}
                </code>
            </label>
            <div style="margin-top: 150px">
                <div class="text-center">
                    <a style="font-size: 20px" class="btn bg-pink" href="{{url('/')}}">REGRESAR</a>
                </div>

            </div>


        </div>
    </div>
    <div class="col-md-4 " style="height : 100vh;">
        <img class="img-responsive" style="position:absolute; bottom:0" src="{{asset('/assets/img/error.jpg')}}" alt="">
    </div>
</div>

</body>

</html>

