<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Oklahoma</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.js') }}"></script>


</head>
<body>
<script type="text/javascript">
    $('img').dragstart( function(event) { event.preventDefault(); });
</script>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a href="{{url('/home')}}" class="navbar-brand">Secretaría de Movilidad</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

</div>
<div class="page-container">


    <div class="page-content">


        <div class="content-wrapper">

            <div class="content">


                <div class="text-center content-group">
                    <br>
                    <img width="300" height="300" src="{{url('img/fuera_servicio.png')}}" alt="">
                    <h1><label for="">Aplicación fuera de servicio.</label></h1><br>



                </div>

                <div class="row">
                    <div class="text-center">

                        <a href="{{url('/login')}}" ><button class="btn bg-pink btn-xlg"><i style="margin-right: 8px" class="glyphicon glyphicon-refresh"></i> Intentar de nuevo </button> </a>

                    </div>
                </div>

            </div>


        </div>


    </div>


</div>

</body>
</html>

