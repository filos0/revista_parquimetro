<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {



        if ($e instanceof \PDOException) {
            return response()->view('errors.error_global', [
                "msg_1"=>  "LA BASE DE DATOS ESTA DURMIENDO zZzZzZ",
                "msg_2"=>  "LLAMA A INFORMATICA PARA DESPERTARLA "]);
        }



        if ($e instanceof HttpException) {

            if ($e instanceof MethodNotAllowedHttpException) {


                if($e instanceof MethodNotAllowedHttpException){
                    return response()->view('errors/error_global',[
                        "msg_1"=>  "NO EXISTE LA DIRECCIÓN SOLICITADA POST/GET",
                        "msg_2"=>  "¿ BUSCAS ALGO ?"]);
                }


            }
            else
                return response()->view('errors/error_global',[
                    "msg_1"=>  "404 - NO EXISTE EL RECURSO SOLICITADO",
                    "msg_2"=>  "¿ BUSCAS ALGO ?"]);


        }
        elseif ($e instanceof TokenMismatchException) {

            return response()->view('errors/sesion_caducada');
        }

        return parent::render($request, $e);
    }
}
