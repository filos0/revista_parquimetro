<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    protected $fillable = [
          'rfc', 'name', 'primer_apellido', 'segundo_apellido', 'password', 'modulo_id', 'estatus_id', 'fecha_logueo','rol' ,'pw'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function modulo(){

        return $this->belongsTo('App\Models\Cat_Modulo_Modelo', 'modulo_id');

    }


}
