<?php

namespace App\Http\Controllers;

use App\Models\Cat_Clave_Vehicular_Modelo;
use App\Models\Permiso_Propietario_Modelo;
use App\Models\Propietario_Modelo;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function pdf_holograma($domicilio, $clave_poligono, $colonia, $propietario, $solicitante, $clave_vehicular, $placa ,$modelo)
    {
        $url_img = asset("/poligonos_imp");
        $url_img = $url_img . "/" . $clave_poligono . ".png";
        $md5 = md5($placa);
        $cadena_qr = "http://esemovi.df.gob.mx/cvp/validar/permisoparquimetro/placa/" . $md5;
        //$cadena_qr = "http://esemovi.df.gob.mx/cvp/validar/permisoparquimetro?folio=".$placa;
        $qrcode = base64_encode(\QrCode::format('png')->size(100)->margin(0)->generate($cadena_qr));


        $view = \View::make('PDF/Holograma', compact('domicilio', 'propietario', 'solicitante', 'clave_vehicular', 'placa', 'clave_poligono', 'colonia', 'url_img', 'qrcode','modelo'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'landscape');
        //return $pdf->stream();

        $output = $pdf->output();

        $nombre_pdf = $placa . '_holograma.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;

    }

    public function prueba($domicilio = "ORIZABA NUM. EXT 25 COLONIA ROMA, CUAUHTEMOC, CDMX ", $clave_poligono = "ROM-3", $colonia = "ROMA", $placa = "161NPH",$modelo = "2018" )
    {

        $propietario = Propietario_Modelo::get()->last();
        $clave_vehicular = Cat_Clave_Vehicular_Modelo::get()->last();
        $solicitante = Permiso_Propietario_Modelo::get()->last();
        $solicitante = json_decode($solicitante->solicitante, true);
        $url_img = asset("/poligonos_imp");
        $url_img = $url_img . "/" . $clave_poligono . ".png";
        $md5 = md5($placa);
        $cadena_qr = "http://esemovi.df.gob.mx/cvp/validar/permisoparquimetro/" . $md5;
        $qrcode = base64_encode(\QrCode::format('png')->size(100)->margin(0)->generate($cadena_qr));


        $view = \View::make('PDF/Holograma', compact('domicilio', 'propietario', 'solicitante', 'clave_vehicular', 'placa', 'clave_poligono', 'colonia', 'url_img', 'qrcode','modelo'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper("letter", 'landscape');

        return $pdf->stream();
    }
    public function pdf()
    {


        $view = \View::make('PDF/Yisus')->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);


        return $pdf->stream();
    }
}
