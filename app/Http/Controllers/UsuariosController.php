<?php

namespace App\Http\Controllers;


use App\User as Usuarios;
use Illuminate\Http\Request;
use App\Models\Cat_Modulo_Modelo as Modulo;


class UsuariosController extends Controller
{

    public function __construct(Modulo $Modulo)
    {
        $this->middleware('auth');
        $this->Modulo = $Modulo;

    }

    public function index()
    {

        $user = \Auth::user();

        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            $usuarios = Usuarios::all();
            $modulos = Modulo::all();

            return view('Admin.Admin_Usuarios')
                ->with('Usuarios', $usuarios)
                ->with('Modulos', $modulos);


        }
    }

    public function baja_usuario(Request $request)
    {
        $user = \Auth::user();

        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 2;
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Usuario Dado de Baja', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }


        }
    }

    public function alta_usuario(Request $request)
    {
        $user = \Auth::user();


        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 1;
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Usuario Dado de Alta', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }
        }
    }

    public function restablecer_contrasena(Request $request)
    {
        $user = \Auth::user();

        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            try {
                $usuario = Usuarios::where('id', $request->modal_usuario)->get()->first();
                $usuario->estatus_id = 3;
                $usuario->password = bcrypt("123456");
                $usuario->save();
                return redirect()->back()->withErrors(array('success', 'Contraseña Restablecida', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'Algo paso :(', ''));
            }
        }
    }


}
