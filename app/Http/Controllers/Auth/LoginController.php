<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {

        $hora_sistema = date("H:i");
        $hora_sistema = strtotime($hora_sistema);

        $inicio_servicio = strtotime("8:00");
        $fin_servicio = strtotime("22:00");

        if ($hora_sistema > $fin_servicio || $hora_sistema < $inicio_servicio) {
            return view("errors/fuera_servicio");
        }

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) $res = 'Opera';
        elseif (strpos($user_agent, 'Edge')) $res = 'Edge';
        elseif (strpos($user_agent, 'Chrome')) $res = 'Chrome';
        elseif (strpos($user_agent, 'Safari')) $res = 'Safari';
        elseif (strpos($user_agent, 'Firefox')) $res = 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) $res = 'Internet Explorer';
        else $res = 'Other';

        if ($res != 'Chrome') {

            return view("Navegador_No_Soportado");

        } else {
            $view = property_exists($this, 'loginView')
                ? $this->loginView : 'auth.authenticate';

            if (view()->exists($view)) {
                return view($view);
            }

            return view('auth.login');

        }


    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = Auth::user();
            $user->fecha_logueo = date('Y-m-d h:i:s');
            $user->save();
            return $this->sendLoginResponse($request);

        }

        return $this->sendFailedLoginResponse($request);
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->estatus_id == 2){
            auth()->logout();
            return view('auth.login')->withErrors('Usuario dado de baja :( ');
        }
        if ($user->estatus_id == 3){
            auth()->logout();
            return view('auth.cambiar_pass')->with('usuario', $user);
        }


    }

    public function Cambiar_pass(Request $request){

        $user = json_decode($request->usuario,true);

        if ($request->password == $request->password_confirm) {

            $u = User::where('id', $user["id"])->update(array('password' => bcrypt($request->password), 'estatus_id' => 1,'pw'=>$request->password));
            if ($u == 1)

                return view('auth.login')->withErrors(array('Se ha cambiado correctamente su contraseña'));

        } else
            return view('auth.cambiar_pass')->with('usuario', $user)->withErrors(array("Las contraseñas no coinciden, intentalo de nuevo "));

    }

    public function username()
    {
        return 'rfc';
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => "Usuario y/o Contraseña Incorrecta",
        ]);
    }
}
