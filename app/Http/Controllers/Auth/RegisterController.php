<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Cat_Modulo_Repositorio as Modulo;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    use RegistersUsers;
    protected $redirectTo = '/';

    public function __construct(Modulo $Modulo)
    {
        $this->Modulo = $Modulo;

        $this->middleware('auth');
    }

    public function showRegistrationForm()
    {
        $user = \Auth::user();

        if ($user->rol == 'DIOS' || $user->rol == 'JEFE')


            return view('auth.register')
                ->with("modulos", $this->Modulo->all());

        else

            return view('errors.error_global')
                ->with('msg_1', 'No tiene permiso para acceder')
                ->with('msg_2', 'Error 403');

    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'rfc' => 'unique:users,rfc'
        ], [
            'rfc.unique' => 'EL RFC ya fue utilizado',
        ]);
    }

    protected function create(array $data)
    {

        //dd($data['rol']);
        try {


            $user = User::create([
                'rfc' => $data['rfc'],
                'name' => $data['nombre'],
                'primer_apellido' => $data['primer_apellido'],
                'segundo_apellido' => $data['segundo_apellido'],
                'password' => bcrypt(123456),
                'modulo_id' => $data['modulo'],
                'rol' => $data['rol'],
                'estatus_id' => 3,
            ]);

            return $user;
        } catch (\Exception $e) {

            report($e);

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }
    }


    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        if (isset($user->id))
            return redirect()->back()->withErrors(array('success', '', 'Usuario Creado Correctamente'));
        else
            return $user;

    }
}
