<?php

namespace App\Http\Controllers;

use App\Repositories\Lote_Material_Repositorio as Lote_Material;
use App\Repositories\Incidencia_Lote_Repositorio as Incidencia;
use App\Repositories\Holograma_Repositorio as Holograma;
use App\Repositories\Cat_CP_Repositorio as CP;
use App\Repositories\Permiso_Propietario_Repositorio as Permiso;
use Illuminate\Support\Facades\Input;
use Auth;
use Request;
class APIController extends Controller
{
    public function __construct(Lote_Material $Lote_Material, Incidencia $Incidencia, Holograma $Holograma, CP $CP,Permiso $Permiso)
    {
        $this->Lote_Material = $Lote_Material;
        $this->Incidencia = $Incidencia;
        $this->Holograma = $Holograma;
        $this->CP = $CP;
        $this->Permiso = $Permiso;
        $this->middleware('auth');
    }

    function impresion()
    {
        if (Request::ajax()) {

            $permiso_id = Input::get('permiso_id');
            $permiso = $this->Permiso->findby('id_permiso_propietario',$permiso_id);

            $permiso->impresion = $permiso->impresion +1;
            $permiso->save();
            return $permiso->impresion;
        }

    }

    function material_modulo()
    {


        if (\Request::ajax()) {

            $modulo_id = Input::get('id_modulo');


            $material = $this->Lote_Material->findAllWhere('id_modulo', $modulo_id);

            if ($material == false)
                return "nada";
            //dd($material);
            return $material;

        }
    }

    function buscar_cp()
    {


        if (\Request::ajax()) {

            $cp = Input::get('cp');

            $poligonos = $this->CP->traer_zonas($cp);
            if ($poligonos->isEmpty())
                return "nada";

            return $poligonos;

        }
    }

    function buscar_clave()
    {


        if (\Request::ajax()) {

            $id_clave = Input::get('id_clave');

            $poligono = $this->CP->findAllWhere('id_cat_cp', $id_clave);

            return $poligono;

        }
    }


    function validar_folio_holograma()
    {


        if (\Request::ajax()) {

            $folio_holograma = Input::get('folio_holograma');
            $impresion_estatus = Input::get('impresion_estatus');
            $permiso_id = Input::get('permiso_id');

            $res = $this->Holograma->findBy('folio_holograma', $folio_holograma);
            $inc = $this->Incidencia->findBy('folio_material', $folio_holograma);

            $lote_material_id = $this->Lote_Material->get_lote_modulo();

            if ($res != false || $inc != false)
                return "folio_ocupado";

            else {
                $folio_disp = $this->Lote_Material->holograma_en_lote($folio_holograma);

                if ($folio_disp == "no_hay_lote")
                    return "no_hay_lote";

                $this->Lote_Material->reducir_lote_modulo();

                if ($impresion_estatus == "SI") {
                    $holograma = $this->Holograma->create([
                        "lote_material_id" => $lote_material_id,
                        "estatus_id" => 1,
                        "folio_holograma" => $folio_holograma,
                        "fecha_holograma" => date('Y-m-d H:i:s'),
                        "vigencia_documento" => date('Y-m-d H:i:s', strtotime('+1 year')),
                        "modulo_id" => Auth::user()->modulo_id,
                        "permiso_propietario_id" => $permiso_id,
                    ]);
                    return "holograma";
                } else {
                    $incidencia = $this->Incidencia->create([
                        "folio_material" => $folio_holograma,
                        "fecha" => date('Y-m-d H:i:s'),
                        "lote_material_id" => $lote_material_id,
                        "usuario_id" => Auth::user()->id,
                    ]);
                    return "incidencia";
                }
            }


        }
    }

    function reporte_hoy()
    {

        $reportes = $this->Permiso->reportes_hoy();
        $total = array();

        if ($reportes != false) {
            foreach ($reportes as $reporte) {
                if ($reporte->holograma)
                    $total[] = array(
                        "Permiso" => $reporte,
                        "Holograma" => $reporte->holograma
                    );

                else
                    $total[] = array(
                        "Permiso" => $reporte,
                        "Holograma" => "-"
                    );
            }
            return $total;
        }
        else
            echo "No han trabajado los bastardos";

    }
}
