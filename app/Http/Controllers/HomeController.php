<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Lote_Material_Repositorio as Lotes;
use App\Repositories\Permiso_Propietario_Repositorio as Permisos;
use Auth;

class HomeController extends Controller
{

    public function __construct(Lotes $Lotes, Permisos $Permisos)
    {

        $this->middleware('auth');
        $this->Permisos = $Permisos;
        $this->Lotes = $Lotes;
    }


    public function index(Request $request)

    {
        $user = \Auth::user();
        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            $fecha = date('Y-m-d');
            $permisos = $this->Permisos->permisos($fecha);
            return view('welcome')->with("permisos",$permisos);
        } else
            return view('welcome');
    }
}
