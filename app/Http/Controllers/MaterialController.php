<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Validator;
use App\Repositories\Cat_Modulo_Repositorio as Modulo;
use App\Repositories\Cat_Tipo_Material_Repositorio as Tipo_Material;
use App\Repositories\Lote_Material_Repositorio as Lote_Material;
use App\Repositories\Incidencia_Lote_Repositorio as Incidencias;


class MaterialController extends Controller
{

    public function __construct(Modulo $Modulo, Tipo_Material $Tipo_Material, Lote_Material $Lote_Material, Incidencias $Incidencias)
    {
        $this->Modulo = $Modulo;
        $this->Tipo_Material = $Tipo_Material;
        $this->Lote_Material = $Lote_Material;
        $this->Incidencias = $Incidencias;
        $this->middleware('auth');

    }


    public function index()
    {
        $user = \Auth::user();


        if ($user->rol == "DIOS" || $user->rol == "JEFE") {

            $modulos = $this->Modulo->all();
            $incidencias = $this->Incidencias->all();
            $tipo_material = $this->Tipo_Material->all();
            return view('Admin.Admin_Material')
                ->with('tipo_material', $tipo_material)
                ->with('incidencias', $incidencias)
                ->with('modulos', $modulos);


        }
    }

    public function agregar_material(Request $request)
    {


        $rango_in = $request->rango_inicial;
        $rango_fin = $request->rango_final;

        $request->rango_inicial = str_pad($rango_in, 6, "0", STR_PAD_LEFT);
        $request->rango_final = str_pad($rango_fin, 6, "0", STR_PAD_LEFT);

        $folios = array();

        if (is_numeric($rango_in) && is_numeric($rango_fin) || $rango_in > $rango_fin) {

            $a = $rango_fin - $rango_in;;
            $b = 0;

            for ($i = 0; $i <= $a; $i++) {
                $n = $rango_in + $b;
                if ($n <= 99999 && $n > 9999)
                    $folios[$b] = '0' . $n;
                elseif ($n <= 9999 && $n > 999)
                    $folios[$b] = '00' . $n;
                elseif ($n <= 999 && $n > 99)
                    $folios[$b] = '000' . $n;
                elseif ($n <= 99 && $n > 9)
                    $folios[$b] = '0000' . $n;
                else
                    $folios[$b] = '00000' . $n;
                $b++;
            }


            if (count($folios) != $request->cantidad)

                return redirect()->back()->withErrors(array('error', 'Cantidad incorrecta', 'La cantidad no coincide con el rango de material proporcionado'));

            else {
                $exite = $this->existencia($folios);

                if ($exite == 1) {

                    return redirect()->back()->withErrors(array('error', 'Folios incorrectos', 'Los folios ya se encuentran dados de alta'));


                } elseif ($exite == 2) {
                    return redirect()->back()->withErrors(array('error', 'Folios incorrectos', 'Los folios ya fueron descartados anteriormente'));
                }
            }
        } else
            return redirect()->back()->withErrors(array('error', 'Error', 'Exixte un problema con los datos, favor de verificarlo'));


        try {

            DB::beginTransaction();

            $this->Lote_Material->create([
                'id_modulo' => $request->id_modulo,
                'cat_tipo_material_id' => $request->tipo_material,
                'fecha_asignacion' => date('Y-m-d'),
                'total_lote' => $request->cantidad,
                'usado' => 0,
                'disponible' => $request->cantidad,
                'numero_rango_inicio' => $request->rango_inicial,
                'numero_rango_fin' => $request->rango_final,
                'estatus' => "A"
            ]);

            DB::commit();
            return redirect()->back()->withErrors(array('success', 'Éxito', 'Material agregado correctamente'));

        } catch (\Exception $e) {
            report($e);
            DB::rollBack();

            return view('errors.error_global')
                ->with('msg', str_limit($e->getMessage()))
                ->with('code', $e->getCode());

        }

    }


    private function existencia($p)
    {

        $inicio = $this->Lote_Material->whereIN('numero_rango_inicio', $p);

        $fin = $this->Lote_Material->whereIN('numero_rango_fin', $p);
        $incidencia = $this->Incidencias->whereIN('folio_material', $p);

        if (!$inicio->isEmpty() || !$fin->isEmpty())
            return 1;

        elseif (!$incidencia->isEmpty())
            return 2;
        else

            return 0;


    }

}


