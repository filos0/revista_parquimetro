<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\PDFController as PDF;
use App\Repositories\Lote_Material_Repositorio as Lote_Material;
use App\Repositories\Permiso_Propietario_Repositorio as Permiso;
use App\Repositories\Propietario_Repositorio as Propietario;
use App\Repositories\Services\Clave_Vehicular as Clave_Vehicular;
use App\Repositories\Services\Informix as Smirnoff;
use App\Repositories\Services\alfred_ws as Alfred_ws;
use App\Repositories\Services\taxi_ws as Taxi_ws;
use App\Repositories\Services\Tenencias;
use App\Repositories\Vehiculo_Repositorio as Vehiculo;


class PermisoController extends Controller
{
    public function __construct(Smirnoff $Smirnoff,
                                Tenencias $Tenencias,
                                Clave_Vehicular $Clave_Vehicular,
                                PDF $PDF,
                                Taxi_ws $Taxi_ws,
                                Alfred_ws $Alfred_ws,
                                Vehiculo $Vehiculo,
                                Lote_Material $Lote_Material,
                                Propietario $Propietario,
                                Permiso $Permiso)
    {
        $this->middleware('auth');
        $this->PDF = $PDF;
        $this->Alfred_ws = $Alfred_ws;
        $this->Taxi_ws = $Taxi_ws;
        $this->Vehiculo = $Vehiculo;
        $this->Smirnoff = $Smirnoff;
        $this->Clave_Vehicular = $Clave_Vehicular;
        $this->Tenencias = $Tenencias;
        $this->Lote_Material = $Lote_Material;
        $this->Propietario = $Propietario;
        $this->Permiso = $Permiso;

    }


    public
    function index()
    {


        try {


            $Material_Disponible = $this->Lote_Material->material_disponible();
            if ($Material_Disponible->isEmpty())
                return view('welcome')->withErrors(array('error', 'No hay hologramas disponibles', ' '));

            return view('Placa');


        } catch (\Exception $e) {
            //report($e);

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }

    }

    public
    function Datos_Vehiculos(Request $request)
    {


        try {

            $permiso = $this->Permiso->findBy("placa", $request->placa);

            if ($permiso != false)
                return redirect()->back()->withErrors(array('error', 'Ya existe un permiso con esta placa', $request->placa));

            $placa_verde = preg_match('/[0-9]{2}E[0-9]{3}/', $request->placa, $matches, PREG_OFFSET_CAPTURE);
            $placa_taxi = preg_match('/[A-Z]{1}[0-9]{4}[A-Z]{1}/', $request->placa, $matches, PREG_OFFSET_CAPTURE);

            if ($placa_verde == 1) {

                $vehiculo = $this->Alfred_ws->consultar_verde($request->placa);

                if (gettype($vehiculo) == "integer") {
                    if ($vehiculo == 404)
                        return redirect()->back()->withErrors(array('error', 'Error !', "No hay datos relacionados con la placa"));
                    if ($vehiculo == 500)
                        return redirect()->back()->withErrors(array('error', 'Error !', "El WebService VERDE no responde, intente de nuevo en un momento "));
                }
            }

            elseif ($placa_taxi == 1) {

                $vehiculo = $this->Taxi_ws->consultar_taxi($request->placa);
                if (gettype($vehiculo) == "integer") {
                    if ($vehiculo == 404)
                        return redirect()->back()->withErrors(array('error', 'Error !', "No hay datos relacionados con la placa"));
                    if ($vehiculo == 500)
                        return redirect()->back()->withErrors(array('error', 'Error !', "El WebService TAXI no responde, intente de nuevo en un momento "));
                }
            }
            else {
                $vehiculo = $this->Smirnoff->consultar_particular($request->placa, 'vehiculo');

                if (empty($vehiculo->vehiculo)) {

                    $vehiculo = $this->Alfred_ws->consultar_antiguo($request->placa);
                    if (gettype($vehiculo) == "integer") {
                        if ($vehiculo == 404)
                            return redirect()->back()->withErrors(array('error', 'Error !', "No hay datos relacionados con la placa"));
                        if ($vehiculo == 500)
                            return redirect()->back()->withErrors(array('error', 'Error !', "El WebService ALFRED no responde, intente de nuevo en un momento "));
                    }

                }
            }

            
            $vehiculo = (object)$vehiculo->vehiculo[0];
           
            $clave_vehicular = $this->Clave_Vehicular->consultar($vehiculo->clave_vehicular_id, $request->placa);

            if (gettype($clave_vehicular) == "integer") {
                if ($clave_vehicular == 500)
                    return redirect()->back()->withErrors(array('error', 'Problemas con la Clave Vehicular ' , $vehiculo->clave_vehicular_id ));
                if ($clave_vehicular == 404)
                    return redirect()->back()->withErrors(array('error', 'La Clave Vehicular ' . $vehiculo->clave_vehicular_id . ' no existe', 'Comunicar con Sistemas'));
            }

            $tenencias = $this->Tenencias->consultar($request->placa);
            if (gettype($tenencias) == "integer")
                return redirect()->back()->withErrors(array('error', 'Sistema de Tenencias no responde', 'Intentelo nuevamente: ' . $tenencias));


            if (!empty($tenencias))
                $tenencias = array_unique($tenencias);
            if (count($tenencias) > 5)
                $tenencias = array_slice($tenencias, 0, 5);

            return view('Datos_Vehiculo')
                ->with('tenencias', $tenencias)
                ->with('vehiculo', $vehiculo)
                ->with('clave_vehicular', $clave_vehicular)
                ->with('placa', $request->placa);


        } catch (\Exception $e) {
            //report($e);


            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }


    }

    public
    function Datos_Generales(Request $request)
    {

        try {

            if ($request->resultado == "ACEPTAR")
                return view('Datos_Generales')
                    ->with('tenencias', $request->tenencias)
                    ->with('vehiculo', $request->vehiculo)
                    ->with('clave_vehicular', $request->clave_vehicular)
                    ->with('placa', $request->placa);
            else
                return redirect(url('/Permiso/Placa'));


        } catch (\Exception $e) {
            //report($e);

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }


    }

    public
    function Datos_Recibe(Request $request)
    {
        try {

            if ($request->resultado == "ACEPTAR") {
                $propietario = array(
                    "primer_apellido" => $request->primer_apellido,
                    "segundo_apellido" => $request->segundo_apellido,
                    "nombre" => $request->nombre,
                    "codigo_postal" => $request->codigo_postal,
                    "delegacion" => $request->delegacion,
                    "colonia" => $request->colonia,
                    "calle" => $request->calle,
                    "num_int" => $request->num_int,
                    "num_ext" => $request->num_ext
                );
                $propietario = json_encode($propietario, true);

                return view('Datos_Recibe')
                    ->with('primer_apellido', $request->primer_apellido)
                    ->with('segundo_apellido', $request->segundo_apellido)
                    ->with('nombre', $request->nombre)
                    ->with('propietario', $propietario)
                    ->with('tenencias', $request->tenencias)
                    ->with('vehiculo', $request->vehiculo)
                    ->with('clave_vehicular', $request->clave_vehicular)
                    ->with('poligono', $request->poligono)
                    ->with('placa', $request->placa);
            } else
                return redirect(url('/Permiso/Placa'));

        } catch (\Exception $e) {
            //report($e);


            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }


    }

    public
    function Finalizar(Request $request)
    {


        try {


            $permiso = $this->Permiso->findBy("placa", $request->placa);
            if ($permiso != false)
                return view('errors.error_global')
                    ->with('msg_1', "Error! Llamar a sistemas")
                    ->with('msg_2', "No debes ACTUALIZAR o REFRESCAR la página");


            if ($request->resultado == "FINALIZAR") {
                $solicitante = array(
                    "primer_apellido" => $request->primer_apellido,
                    "segundo_apellido" => $request->segundo_apellido,
                    "nombre" => $request->nombre,
                    "parentesco" => $request->parentesco,
                    "identificacion" => $request->identificacion,
                    "telefono" => $request->telefono,
                    "comentarios" => $request->comentarios,

                );
                $propietario = json_decode($request->propietario, true);

                if ($propietario["num_int"] == "")
                    $domicilio = $propietario["calle"] . ", NÚM. EXT. " . $propietario["num_ext"] . ", COLONIA " . strtoupper($propietario["colonia"]) . ", DELEGACIÓN " . strtoupper($propietario["delegacion"]) . ", CÓDIGO POSTAL " . $propietario["codigo_postal"] . ", CDMX";
                else
                    $domicilio = $propietario["calle"] . ", NÚM. EXT. " . $propietario["num_ext"] . ", NÚM. INT. " . $propietario["num_int"] . ", COLONIA " . strtoupper($propietario["colonia"]) . ", DELEGACIÓN " . strtoupper($propietario["delegacion"]) . ", CÓDIGO POSTAL " . $propietario["codigo_postal"] . ", CDMX";


                $vehiculo = json_decode($request->vehiculo, true);
                $tenencias = json_decode($request->tenencias, true);
                $clave_vehicular = json_decode($request->clave_vehicular, true);


                DB::beginTransaction();

                $propietario_ben = $this->Propietario->create([
                    'primer_apellido' => $propietario["primer_apellido"],
                    'segundo_apellido' => $propietario["segundo_apellido"],
                    'nombre' => $propietario["nombre"],
                    'calle' => $propietario["calle"],
                    'num_int' => $propietario["num_int"],
                    'num_ext' => $propietario["num_ext"],
                    'cat_cp_id' => $request->poligono,
                ]);

                $vehiculo = $this->Vehiculo->create([
                    "serie_vehicular" => $vehiculo["serie_vehicular"],
                    "clave_vehicular" => $vehiculo["clave_vehicular_id"],
                    "modelo" => $vehiculo["modelo"],
                    "numero_repuve" => $vehiculo["numero_repuve"],
                    "numero_puertas" => $vehiculo["numero_puertas"],
                    "numero_motor" => $vehiculo["numero_motor"],
                    "numero_cilindros" => $vehiculo["numero_cilindros"],
                    "tipo_servicio_id" => $vehiculo["tipo_servicio_id"],
                    "uso_vehiculo_id" => $vehiculo["uso_vehiculo_id"],
                    "clase_tipo_vehiculo_id" => $vehiculo["clase_tipo_vehiculo_id"],
                    "pais_id" => $vehiculo["pais_id"]
                ]);


                $permiso = $this->Permiso->create([
                    'fecha_permiso' => date('Y-m-d H:i:s'),
                    'modulo_id' => Auth::user()->modulo_id,
                    'users_id' => Auth::user()->id,
                    'solicitante' => json_encode($solicitante, true),
                    'placa' => $request->placa,
                    'vehiculo_id' => $vehiculo->id_vehiculo,
                    'propietario_id' => $propietario_ben->id_propietario,
                    'md5' => md5($request->placa),
                ]);
                DB::commit();
                $colonia = $propietario_ben->zona->colonia;
                $clave_poligono = $propietario_ben->zona->clave;

                $pdf = $this->PDF->pdf_holograma($domicilio, $clave_poligono, $colonia, $propietario, $solicitante, $clave_vehicular, $request->placa, $vehiculo["modelo"]);

                return view('Finalizar')
                    ->with('pdf', $pdf)
                    ->with('propietario', $propietario)
                    ->with('domicilio', $domicilio)
                    ->with('tenencias', $request->tenencias)
                    ->with('vehiculo', $request->vehiculo)
                    ->with('clave_vehicular', $request->clave_vehicular)
                    ->with('permiso_id', $permiso->id_permiso_propietario)
                    ->with('placa', $request->placa);
            } else
                return redirect(url('/Permiso/Placa'));


        } catch (\Exception $e) {
            report($e);
            DB::rollBack();

            return view('errors.error_global')
                ->with('msg_1', str_limit($e->getMessage()))
                ->with('msg_2', $e->getCode());

        }


    }


    public function tramite(Request $request)
    {
        $user = \Auth::user();

        if ($user->rol == "DIOS" || $user->rol == "JEFE") {

            $tramite = $this->Permiso->permiso_placa_fecha($request->placa, $request->fecha_alta);

            if ($tramite->isEmpty())
                return redirect()->back()->withErrors(array('error', 'No hay ningún trámite ', 'Verifica los datos'));
            else
                return view("Admin.Tramite")->with("tramite", $tramite->first());
        }

    }

    public function tramite_cancelar(Request $request)
    {
        $user = \Auth::user();

        if ($user->rol == "DIOS" || $user->rol == "JEFE") {
            try {

                $tramite = $this->Permiso->findBy("id_permiso_propietario", $request->id_revista);
                $tramite->observacion = "Cancelación por oficio: " . $request->folio_oficio_;
                $tramite->deleted_at = date('Y-m-d h:i:s');
                $tramite->save();
                return redirect()->back()->withErrors(array('success', 'Trámite Cancelado', ''));
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(array('error', 'No se pudo cancelar el oficio :(', 'Hazlo manualmente'));
            }


        }
    }

}
