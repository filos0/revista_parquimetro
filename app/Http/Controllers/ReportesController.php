<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Services\Informix as Smirnoff;
use App\Repositories\Services\Clave_Vehicular as Clave_Vehicular;
use App\Repositories\Services\Tenencias;
use App\Http\Controllers\PDFController as PDF;
use App\Repositories\Lote_Material_Repositorio as Lote_Material;
use App\Repositories\Permiso_Propietario_Repositorio as Permiso;
use App\Repositories\Propietario_Repositorio as Propietario;
use App\Repositories\Incidencia_Lote_Repositorio as Incidencias;
use DB;
use Auth;


class ReportesController extends Controller
{
    public function __construct(Smirnoff $Smirnoff,
                                Tenencias $Tenencias,
                                Clave_Vehicular $Clave_Vehicular,
                                PDF $PDF,
                                Incidencias $Incidencias,
                                Lote_Material $Lote_Material,
                                Propietario $Propietario,
                                Permiso $Permiso)
    {
        $this->middleware('auth');
        $this->PDF = $PDF;
        $this->Smirnoff = $Smirnoff;
        $this->Clave_Vehicular = $Clave_Vehicular;
        $this->Tenencias = $Tenencias;
        $this->Lote_Material = $Lote_Material;
        $this->Propietario = $Propietario;
        $this->Permiso = $Permiso;
        $this->Incidencias = $Incidencias;

    }

    public function hoy(Request $request)
    {
        $user = Auth::user();


        if ($user->rol == "DIOS" || $user->rol == "JEFE") {


            $Permisos = $this->Permiso->reportes_hoy();

            return view('Reportes/Reporte_Hoy')
                ->With('Permisos', $Permisos)
                ->With('Incidencias', array());
        }
    }
}
