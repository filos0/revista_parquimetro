<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incidencia_Lote_Modelo extends Model
{


    protected $table = 'incidencia_lote';

    protected $fillable = ['id_incidencia_lote', 'folio_material', 'fecha', 'lote_material_id', 'usuario_id'];

    protected $primaryKey = 'id_incidencia_lote';

    public $timestamps = false;
}
