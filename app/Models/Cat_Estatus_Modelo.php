<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat_Estatus_Modelo extends Model
{

    protected $table = 'cat_estatus';

    protected $fillable = ['clave_estatus', 'estatus'];

    public $timestamps = false;
}
