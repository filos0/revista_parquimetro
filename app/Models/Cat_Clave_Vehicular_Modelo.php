<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat_Clave_Vehicular_Modelo extends Model
{


    protected $table = 'cat_clave_vehicular';

    protected $fillable = ['id_cat_clave_vehicular', 'clave_vehicular', 'digito', 'marca', 'linea', 'version'];

    protected $primaryKey = 'id_cat_clave_vehicular';

    public $timestamps = false;

}
