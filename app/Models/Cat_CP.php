<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat_CP extends Model
{


    protected $table = 'cat_cp';

    protected $fillable = ['id_cat_cp', 'cp', 'colonia', 'delegacion', 'clave' ];

    protected $primaryKey = 'id_cat_cp';

    public $timestamps = false;

}
