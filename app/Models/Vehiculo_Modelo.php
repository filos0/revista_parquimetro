<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehiculo_Modelo extends Model
{
    protected $table = 'vehiculo';

    protected $fillable = [
        "serie_vehicular",
        "clave_vehicular",
        "modelo",
        "numero_repuve",
        "numero_puertas",
        "numero_motor",
        "numero_cilindros",
        "tipo_servicio_id",
        "uso_vehiculo_id",
        "clase_tipo_vehiculo_id",
        "pais_id"];

    protected $primaryKey = 'id_vehiculo';
    public $timestamps = false;
}


