<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evento_Modelo extends Model
{

    protected $table = 'evento';

    protected $fillable = ['id_evento', 'users_id', 'placa', 'ip', 'evento', 'fecha'];

    protected $primaryKey = 'id_evento';

    public $timestamps = false;

}
