<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cat_Modulo_Modelo extends Model
{

    protected $table = 'cat_modulo';

    protected $fillable = ['clave_modulo', 'modulo', 'cat_estatus_id'];

    protected $primaryKey = 'id_modulo';

    public $timestamps = false;

      public function user(){

        return $this->hasMany('App\Models\Revista\User');
    }

    public function lote_material(){

        return $this->hasMany('App\Models\Lote_Material_Modelo','id_modulo');
    }


}
