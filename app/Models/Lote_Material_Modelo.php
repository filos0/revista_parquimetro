<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lote_Material_Modelo extends Model
{

    protected $table = "lote_material";
    protected $fillable = ['id_lote_material', 'id_modulo', 'cat_tipo_material_id', 'fecha_asignacion', 'total_lote',
        'usado', 'disponible', 'numero_rango_inicio', 'numero_rango_fin', 'estatus'];

    protected $primaryKey = 'id_lote_material';

    public $timestamps = false;


    public function modulo(){

        return $this->belongsTo('App\Models\Revista\Cat_moduloModel','id_modulo');
    }

    public function  tipo_material(){

        return $this->belongsTo('App\Models\Revista\Cat_tipoMaterialModelo','cat_tipo_material_id');
    }


}
