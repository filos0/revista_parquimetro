<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Holograma_Modelo extends Model
{


    protected $table = 'holograma';

    protected $fillable = ['id_holograma', 'lote_material_id', 'estatus_id', 'folio_holograma', 'fecha_holograma', 'vigencia_documento', 'modulo_id', 'permiso_propietario_id'];

    protected $primaryKey = 'id_holograma';

    public $timestamps = false;
}
