<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso_Propietario_Modelo extends Model
{

    use SoftDeletes;

    protected $table = 'permiso_propietario';

    protected $fillable = ['fecha_permiso', 'modulo_id', 'users_id', 'solicitante', 'placa', 'propietario_id','vehiculo_id', 'md5'];

    protected $primaryKey = 'id_permiso_propietario';

    public $timestamps = true;

    protected $dates = ['deleted_at'];


    public function holograma()
    {
        return $this->hasOne('\App\Models\Holograma_Modelo', 'permiso_propietario_id');
    }

    public function modulo()
    {
        return $this->belongsTo('\App\Models\Cat_Modulo_Modelo', 'modulo_id');
    }

    public function usuario()
    {
        return $this->belongsTo('\App\User', 'users_id');
    }

    public function Vehiculo()
    {

        return $this->belongsTo('App\Models\Vehiculo_Modelo', 'vehiculo_id');
    } public function Propietario()
{

    return $this->belongsTo('App\Models\Propietario_Modelo', 'propietario_id');
}
}
