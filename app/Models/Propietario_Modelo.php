<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propietario_Modelo extends Model
{


    protected $table = 'propietario';

    protected $fillable = [ 'primer_apellido', 'segundo_apellido', 'nombre', 'cat_cp_id','calle','num_ext','num_int'];

    protected $primaryKey = 'id_propietario';

    public $timestamps = false;

    public function zona(){
        return $this->belongsTo('App\Models\Cat_CP','cat_cp_id');
    }
}
