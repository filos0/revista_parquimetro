<?php

namespace App\Repositories;


use App\Repositories\Repositorio;

class Cat_Tipo_Material_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Cat_Tipo_Material_Modelo';
    }
}