<?php
namespace App\Repositories;



class Cat_CP_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Cat_CP';
    }

    public function traer_zonas( $cp)
    {
        $faw = $this->model->where("cp", '=', $cp)->orderBy("orden",'asc')->get();

        if (!is_null($faw)) {
            return $faw;
        } else {
            return false;
        }
    }

}