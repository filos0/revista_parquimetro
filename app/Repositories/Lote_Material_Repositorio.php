<?php

namespace App\Repositories;

use Auth;


class Lote_Material_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Lote_Material_Modelo';
    }

    function material_disponible()
    {
        $material = $this->model->where('id_modulo', Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get();
        return $material;
    }

    function folio_existente()
    {
        $material = $this->model->all();
        return $material;
    }

    public function get_lote_modulo()
    {
        $lote = $this->model->where('id_modulo', Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        return $lote->id_lote_material;
    }

    public function reducir_lote_modulo()
    {
        $lote = $this->model->where('id_modulo', \Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        $lote->usado = $lote->usado + 1;
        $lote->disponible = $lote->disponible - 1;
        $lote->save();
    }

    public function holograma_en_lote($holograma)
    {
        $lote = $this->model->where('id_modulo', Auth::user()->modulo_id)->where('disponible', '>', '0')->where('estatus', 'A')->get()->first();
        $rango_in = $lote->numero_rango_inicio;
        $rango_fin = $lote->numero_rango_fin;
        $folios = array();
        $a = 0;

        foreach (range($rango_in, $rango_fin) as $folio) {
            if ($folio <= 99999 && $folio > 9999)
                $folios[] = '0' . $folio;
            elseif ($folio <= 9999 && $folio > 999)
                $folios[] = '0' . $folio;
            elseif ($folio <= 999 && $folio > 99)
                $folios[] = '00' . $folio;
            elseif ($folio <= 99 && $folio > 9)
                $folios[] = '000' . $folio;
            else
                $folios[] = '0000' . $folio;
        }


        if (!in_array($holograma, $folios))
            return "no_hay_lote";
        else
            return "folio_disponible";


    }


}