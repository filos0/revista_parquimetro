<?php
namespace App\Repositories;



class Cat_Modulo_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Cat_Modulo_Modelo';
    }

    public function modulos_x_nombre(){
        return $this->model->orderBy('modulo', 'asc')->get();

    }

}