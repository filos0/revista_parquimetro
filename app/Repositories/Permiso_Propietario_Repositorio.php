<?php

namespace App\Repositories;


class Permiso_Propietario_Repositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Permiso_Propietario_Modelo';
    }

    function reportes_hoy()
    {
        $hoy = date('Y-m-d');
        $reportes = $this->model->where('fecha_permiso', 'LIKE', $hoy . ' %')->get();

        if ($reportes->isEmpty()) {

            return false;

        } else {

            return $reportes;

        }
    }
    function permisos($fecha){
        $permisos = $this->model->whereDate('fecha_permiso', $fecha )->get();

        if ($permisos->isEmpty()) {

            return null;

        } else {

            return $permisos;

        }
    }
    function permiso_placa_fecha($placa,$fecha){
        $permisos = $this->model->whereDate('fecha_permiso', $fecha )->where("placa",$placa)->get();

            return $permisos;

    }

}