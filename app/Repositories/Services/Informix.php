<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;


use Illuminate\Support\Facades\DB;
use function Sodium\version_string;

class Informix
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://128.222.200.41:8888',
            'timeout' => 15,
            'connect_timeout'=> 15
        ]);


    }


    public function consultar_particular($placa_serie, $parte ='todo')
    {
        //tramite
        //vehiculo
        //propietario


        try {
            $placa_serie = trim(strtoupper($placa_serie));
            $response = $this->client->request('POST', '/smeargle/consulta/'.$parte,['json' => ["consulta" => $placa_serie]]);
            $status = $response->getStatusCode();


            if($status == 200){

                return json_decode($response->getBody()->getContents());
            }
            else{

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }




    }



}