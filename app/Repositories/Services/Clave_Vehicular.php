<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:30 PM
 */

namespace App\Repositories\Services;

use GuzzleHttp\Client;
use App\Models\Cat_Clave_Vehicular_Modelo as clave;

class Clave_Vehicular
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://128.222.200.178:8252',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);

        $this->client_antiguo = new Client([
            'base_uri' => 'http://128.222.200.41:7777',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);

    }

    public function consultar($clave_vehicular, $placa)
    {

        try {

            $clv = clave::where('clave_vehicular', $clave_vehicular)->first();

            if (!$clv) {
                $response = $this->client->post('/ClaveVehicularFinanzas', ['json' => ["numero" => $clave_vehicular]]);
                $status = $response->getStatusCode();

                if ($status == 200) {

                    $result = json_decode($response->getBody()->getContents());
                    if ($result == null)
                        return 500;
                    if ($result->descripcionModelo == "null" || $result->descripcionLinea == "null" || $result->descripcionMarca == "null" || $result->claveCarga == "null" || $result->claveDivision == "null" || $result->claveLinea == "null" || $result->claveMarca == "null" || $result->claveDigito == "null") {

                        $response = $this->client_antiguo->post('/alfred/vehicles/type/antiguo', ['json' => ["service" => "vehicular_key", "vehicle" => ["plate" => $placa]]]);
                        $result = json_decode($response->getBody()->getContents());

                        if ($result[0]->errMsg == "Not Found" && $result[0]->errCode == 1)
                            return 404;
                        else
                            return $this->insertar_clave_vehicular($clave_vehicular, $result, "antiguo");
                    } else
                        return $this->insertar_clave_vehicular($clave_vehicular, $result);
                } else

                    return 500;

            }
            return $clv;

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }

    }

    public function insertar_clave_vehicular($clave_vehicular, $result, $bandera = "ninguna")
    {

        if ($bandera == "ninguna")
            $clv = clave::create(['clave_vehicular' => $clave_vehicular, 'digito' => $result->claveDigito, 'marca' => $result->descripcionMarca,
                'linea' => $result->descripcionLinea, 'version' => $result->descripcionModelo]);
        else
            $clv = clave::create(['clave_vehicular' => $clave_vehicular, 'digito' => 0, 'marca' => $result[0]->mark,
                'linea' => $result[0]->line, 'version' => $result[0]->model]);
        return $clv;
    }
}