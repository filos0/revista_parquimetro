<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;


use Illuminate\Support\Facades\DB;

class taxi_ws
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://128.222.200.52:8406',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);


    }


    public function consultar_taxi($placa)
    {

        try {
            $placa = trim(strtoupper($placa));
            $response = $this->client->post('/taxi/datos/concesion', ['json' => ["placa" => $placa]]);
            $status = $response->getStatusCode();


            if ($status == 200) {

                $data = json_decode($response->getBody()->getContents());

                if ($data->errCode == 1 && $data->errMsg = "Datos de la concesión, no localizados")
                    return 404;
                else {

                    $modelo = array(
                        'vehiculo' => array(
                            [
                                "numero_factura" => null,
                                "clave_vehicular_id" => $data->CLAVEVEHICULAR,
                                "estatus_id" => null,
                                "distribuidora" => null,
                                "fecha_alta" => null,
                                "folio_documento_legalizacion" => null,
                                "capacidad_litros" => ($data->CANTIDADCOMBUSTIBLE) ?? null,
                                "fecha_factura" => null,
                                "aseguradora" => null,
                                "numero_motor" => ($data->MOTOR) ?? null,
                                "numero_personas" => ($data->PASAJEROS) ?? null,
                                "tipo_combustible_id" => null,
                                "numero_puertas" => null,
                                "numero_poliza_seguro" => null,
                                "fecha_documento_legalizacion" => null,
                                "importe_factura" => null,
                                "capacidad_kwh" => null,
                                "modelo" => $data->MODELO,
                                "tipo_servicio_id" => null,
                                "uso_vehiculo_id" => null,
                                "numero_cilindros" => ($data->CILINDROS) ?? null,
                                "numero_repuve" => null,
                                "serie_vehicular" => $data->SERIE,
                                "origen_motor" => null,
                                "pais_id" => null,
                                "clase_tipo_vehiculo_id" => null
                            ]
                        )
                    );

                    return (object)$modelo;
                }

            } else {

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }


    }


}