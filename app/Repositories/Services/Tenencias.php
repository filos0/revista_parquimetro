<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:30 PM
 */

namespace App\Repositories\Services;

use GuzzleHttp\Client;

class Tenencias
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://128.222.200.178:8124',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);

    }

    public function consultar($placa)
    {

        try {
            $response = $this->client->request('POST', '/tenencias', ['json' => ["placa" => $placa]]);

            $status = $response->getStatusCode();


            if ($status == 200) {
                $response = json_decode($response->getBody()->getContents());
                $periodos = array();

                foreach ($response as $pagos)
                    array_push($periodos, $pagos->perini);

                arsort($periodos);

                return $periodos;

            } else {

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }
    }
}