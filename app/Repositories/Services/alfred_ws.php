<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;


use Illuminate\Support\Facades\DB;
use function Sodium\version_string;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;

class alfred_ws
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://128.222.200.41:7777',
            'timeout' => 15,
            'connect_timeout' => 15
        ]);


    }


    public function consultar_antiguo($placa)
    {

        try {
            $placa = trim(strtoupper($placa));
            $response = $this->client->post('/alfred/vehicles/type/antiguo',['json' => ["service" => "vehicle", "vehicle" => ["plate" => $placa]] ]);
            $status = $response->getStatusCode();


            if ($status == 200) {

                $vehiculo = json_decode($response->getBody()->getContents());
                if ($vehiculo[0]->errMsg == "Not Found" && $vehiculo[0]->errCode == 1)
                    return 404;

                $vehiculo = $this->reconstruir($vehiculo[0]);
                
                return (object)$vehiculo;
            } else {
                return 500 ;


            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

             return 500 ;

        }


    }
    public function consultar_verde($placa)
    {

        try {
            $placa = trim(strtoupper($placa));
            $response = $this->client->post('/alfred/vehicles/type/verde',['json' => ["service" => "vehicle", "vehicle" => ["plate" => $placa]] ]);
            $status = $response->getStatusCode();


            if ($status == 200) {

                $vehiculo = json_decode($response->getBody()->getContents());
                if ($vehiculo[0]->errMsg == "Not Found" && $vehiculo[0]->errCode == 1)
                    return 404;

                $vehiculo = $this->reconstruir($vehiculo[0]);

                return (object)$vehiculo;
            } else {
                return 500 ;


            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

             return 500 ;

        }


    }

    private function reconstruir($vehiculo)
    {
        $modelo = array(
            'vehiculo' => array(
                [
                    "aseguradora" => null,
                    "capacidad_kwh" => null,
                    "capacidad_litros" => null,
                    "clase_tipo_vehiculo_id" => null,
                    "clave_vehicular_id" => $vehiculo->vehicular_key,
                    "distribuidora" => null,
                    "estatus_id" => ($vehiculo->status) ?? null,
                    "fecha_alta" => ($vehiculo->date_up) ?? null,
                    "fecha_documento_legalizacion" => null,
                    "fecha_factura" => null,
                    "folio_documento_legalizacion" => null,
                    "importe_factura" => null,
                    "modelo" => $vehiculo->model,
                    "numero_cilindros" => ($vehiculo->number_cylinders) ?? null,
                    "numero_factura" => null,
                    "numero_motor" => ($vehiculo->motor_number) ?? null,
                    "numero_personas" => ($vehiculo->number_passenger) ?? null,
                    "numero_poliza_seguro" => null,
                    "numero_puertas" => ($vehiculo->number_doors) ?? null,
                    "numero_repuve" => ($vehiculo->repuve) ?? null,
                    "origen_motor" => null,
                    "pais_id" => null,
                    "serie_vehicular" => $vehiculo->vin,
                    "tipo_combustible_id" => null,
                    "tipo_servicio_id" => null,
                    "uso_vehiculo_id" => null,
                ]
            )
        );
        return $modelo;
    }

}