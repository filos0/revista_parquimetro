<?php

Auth::routes();

Route::get('/','HomeController@index');
Route::get('/home', 'HomeController@index');

Route::get('/Permiso/Placa','PermisoController@index')->name('index');
Route::post('/Permiso/Datos_Vehiculos','PermisoController@Datos_Vehiculos');
Route::post('/Permiso/Datos_Generales','PermisoController@Datos_Generales');
Route::post('/Permiso/Datos_Recibe','PermisoController@Datos_Recibe');
Route::post('/Permiso/Finalizar','PermisoController@Finalizar');


Route::post('/Cambiar/Constraseña','Auth\LoginController@Cambiar_pass');


Route::get('/Reportes/Hoy','ReportesController@hoy');
Route::get('/pdf/prueba','PDFController@prueba');

Route::get('/pdf','PDFController@pdf');



Route::get('/Tramite', function (){return view("Admin.Tramite");});
Route::post('/Tramite','PermisoController@tramite');
Route::post('/Tramite/Cancelar','PermisoController@tramite_cancelar');


Route::get('/Usuarios', 'UsuariosController@index');
Route::post('/Usuario/baja', 'UsuariosController@baja_usuario');
Route::post('/Usuario/alta', 'UsuariosController@alta_usuario');
Route::post('/Usuario/restablecer_contraseña', 'UsuariosController@restablecer_contrasena');



Route::get('/Material','MaterialController@index');
Route::post('/Material/Agregar', 'MaterialController@agregar_material');





Route::post('/API/Buscar/Material', 'APIController@material_modulo');
Route::post('/API/Validar_Folio_Holograma/', 'APIController@validar_folio_holograma');
Route::post('/API/Buscar/CP', 'APIController@buscar_cp');
Route::post('/API/Buscar/Clave', 'APIController@buscar_clave');
Route::post('/API/Impresion/', 'APIController@impresion');

/*Route::get('/API/reporte', 'APIController@reporte_hoy');*/







