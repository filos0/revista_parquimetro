$(document).ready(function () {

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });

    $(':input').addClass('text-uppercase')
        .focus(function () {$(this).css("box-shadow", "0 0 0 500px rgba(0, 187, 236, 0.4) inset");})
        .blur(function () {$(this).css("box-shadow", "none");});


    $('.mayusculas').keyup(function () {
        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ\s.,-]+/);
        $(this).val(cadena);

    });
    $('.placa_text').keyup(function () {
        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);

    });
    $('.alfanumerico').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ@\s.,-]+/);
        $(this).val(cadena);
    });
    $('.folio_text').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);
    });
    $('.numeros').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9]+/);
        $(this).val(cadena);

    });

});


function fecha_actual() {
    moment.locale('es');
    var fecha = moment().format('L') + " " + moment().format('LTS');
    $('#hora').text(fecha);
}

setInterval(fecha_actual, 1000);